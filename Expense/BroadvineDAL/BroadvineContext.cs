﻿using Broadvine.Models.Common;
using Broadvine.Models.Expense;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Broadvine.DAL
{
    public class BroadvineContext  : DbContext
    {
        public BroadvineContext() : base("BroadvineContext") 
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Workflow> Workflow { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}