﻿using System.ComponentModel.DataAnnotations;

namespace Broadvine.Membership
{
    public class RolePermission
    {
        [Key]
        public int RolePermissionID { get; set; }
        public int RoleID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
