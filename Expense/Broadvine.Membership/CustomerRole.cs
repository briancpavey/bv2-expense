﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Broadvine.Membership
{
    public class CustomerRole : IdentityRole
    {
        public int? CustomerID { get; set; }
    }
}
