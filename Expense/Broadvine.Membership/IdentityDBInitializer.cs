﻿using System.Data.Entity;

namespace Broadvine.Membership
{
    public class IdentityDBInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);

            //  BrianP: MySQL Support OOB Research
            //context.Database.ExecuteSqlCommand(@"CREATE TABLE IF NOT EXISTS `aspnetusers` (
            //                                      `Id` varchar(128) NOT NULL,
            //                                      `Email` varchar(256) DEFAULT NULL,
            //                                      `EmailConfirmed` tinyint(1) NOT NULL,
            //                                      `PasswordHash` longtext,
            //                                      `SecurityStamp` longtext,
            //                                      `PhoneNumber` longtext,
            //                                      `PhoneNumberConfirmed` tinyint(1) NOT NULL,
            //                                      `TwoFactorEnabled` tinyint(1) NOT NULL,
            //                                      `LockoutEndDateUtc` datetime DEFAULT NULL,
            //                                      `LockoutEnabled` tinyint(1) NOT NULL,
            //                                      `AccessFailedCount` int(11) NOT NULL,
            //                                      `UserName` varchar(256) NOT NULL,
            //                                      PRIMARY KEY (`Id`)
            //                                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
        }
    }

}
