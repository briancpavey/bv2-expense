﻿using System.ComponentModel.DataAnnotations;

namespace Broadvine.Membership
{
    public class Permissions
    {
        [Key]
        public int PermissionID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
