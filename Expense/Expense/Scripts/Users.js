﻿$(document).ready(function () {

    if ($("#btn-reset-password") != null && $("#btn-reset-password").length > 0) {
        $("#btn-reset-password").click(function (e) {
            e.preventDefault();
            $("#dialog-confirm-password-reset").dialog("open");
        });
    }

    $("#dialog-confirm-password-reset").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Yes": function () {
                var userId = $("#Id").val();
                var userName = $("#UserName").val();

                $.post("/Users/ResetPassword", { "userId": userId, "userName": userName }, function (d) {
                    $("#dialog-message-text").empty().html(d.message);
                    $("#dialog-message").dialog({
                        title: "Password Reset...",
                        close: function (event, ui) {
                        }
                    }).dialog("open");
                })

                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#btn-reset-mypassword").click(function (e) {
        e.preventDefault();
        var password1 = $("#password-1").val();
        var password2 = $("#password-2").val();
        var userId = $("#Id").val();
        var userName = $("#UserName").val();

        $.post("/Users/ResetMyPassword", { "userId": userId, "userName": userName, "password1" : password1, "password2" : password2 }, function (d) {
            $("#dialog-message-text").empty().html(d.message);
            $("#dialog-message").dialog({
                title: "Password Reset...",
                close: function (event, ui) {
                }
            }).dialog("open");
        })

    });

    $("#dialog-message").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#UserRoles").kendoMultiSelect().data("kendoMultiSelect");

});