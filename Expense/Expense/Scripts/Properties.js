﻿$(document).ready(function () {
    function RefreshPropertyGrid() {

        $("#gridProperties").kendoGrid({
            dataSource: {
                autoBind: false,
                type: "json",
                serverPaging: true,
                serverSorting: true,
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/propertiesapi",
                        dataType: "json",
                        //data: {
                            
                        //},
                    }
                },
                schema: { total: "total", data: "data" },
            },
            sortable: {
                mode: "single",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 4
            },
            columns: [
            {
                field: "Code",
                title: "Code",
                width: 105,
            },
            {
                field: "Name",
                title: "Name",
            },
            {
                field: "Address1",
                title: "Address",
            },
            {
                field: "City",
                title: "City",
            },
            {
                field: "InvoiceCount",
                title: "Total Invoices",
                width: 150
            },
            //{
            //    command: [
            //        {
            //            name: "Details",
            //            click: showDetails
            //        }
            //    ],
            //    width: 100
            //}
            ],
        });


        //wnd = $("#details")
        //            .kendoWindow({
        //                title: "Property Details",
        //                modal: true,
        //                visible: false,
        //                resizable: false,
        //                width: 300
        //            }).data("kendoWindow");

        //detailsTemplate = kendo.template($("#template").html());

        //function showDetails(e) {
        //    e.preventDefault();

        //    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        //    wnd.content(detailsTemplate(dataItem));
        //    wnd.center().open();
        //}
    }

    RefreshPropertyGrid();
});