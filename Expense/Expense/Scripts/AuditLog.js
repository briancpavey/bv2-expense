﻿$(document).ready(function () {
    RefreshActivityGrid();
});

function RefreshActivityGrid() {
    if ($("#gridInvoice") != null) {
        $("#gridInvoice").kendoGrid({
            dataSource: {
                autoBind: false,
                type: "json",
                serverPaging: true,
                serverSorting: true,
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/auditlogapi",
                        dataType: "json",
                        data: {
                            "sourceId": 0, "typeId": 0
                        },
                    }
                },
                schema: { total: "total", data: "data" },
            },
            sortable: {
                mode: "single",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 4
            },
            columns: [
                {
                    field: "DateTimeCreated",
                    title: "Date & Time",
                    width: 155,
                    template: "#= kendo.toString(kendo.parseDate(DateTimeCreated, 'yyyy-MM-ddTHH:mm:ss'), 'g')#"
                },
                {
                    field: "InvoiceNumber",
                    title: "Invoice #",
                    width: 150
                },
                {
                    field: "PropertyName",
                    title: "Property",
                },
                {
                    field: "Source",
                    title: "User",
                },
                {
                    field: "Status",
                    title: "Status",
                    width: 100
                },
            ],
        });
    }

    if ($("#gridSystem") != null) {
        $("#gridSystem").kendoGrid({
            dataSource: {
                autoBind: false,
                type: "json",
                serverPaging: true,
                serverSorting: true,
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/auditlogapi",
                        dataType: "json",
                        data: {
                            "sourceId": 9, "typeId": 0
                        },
                    }
                },
                schema: { total: "total", data: "data" },
            },
            sortable: {
                mode: "single",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 4
            },
            columns: [
                {
                    field: "DateTimeCreated",
                    title: "Date & Time",
                    width: 155,
                    template: "#= kendo.toString(kendo.parseDate(DateTimeCreated, 'yyyy-MM-ddTHH:mm:ss'), 'g')#"
                },
                {
                    field: "Source",
                    title: "Source",
                },
                {
                    field: "Status",
                    title: "Operation",
                },
            ],
        });
    }
}


