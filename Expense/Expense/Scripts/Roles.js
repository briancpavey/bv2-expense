﻿$(document).ready(function () {
    function RefreshRolesGrid() {

        $("#gridRoles").kendoGrid({
            dataSource: {
                autoBind: false,
                type: "json",
                serverPaging: true,
                serverSorting: true,
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/rolesapi",
                        dataType: "json",
                    }
                },
                schema: { total: "total", data: "data" },
            },
            sortable: {
                mode: "single",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 4
            },
            columns: [
            {
                field: "Name",
                title: "Name",
            },
            //{
            //    command: [
            //        {
            //            name: "Details",
            //            click: showDetails
            //        }
            //    ],
            //    width: 100
            //}
            ],
        });
    }

    RefreshRolesGrid();
});