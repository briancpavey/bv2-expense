﻿$(document).ready(function () {
    $("#dialog-message").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#searchProperty").kendoComboBox({
        dataTextField: "label",
        dataValueField: "value",
        filter: "contains",
        autoBind: true,
        autoSync: true,
        delay: 500,
        suggest: true,
        dataSource:
        {
            serverFiltering: true,
            type: "json",
            transport: {
                read: { url: "/api/PropertiesAPI", cache: false },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return { term: document.activeElement.value }       //-   Use for combobox
                    }
                }
            }
        },
        select: function (e) {
            PopulatePropertyInformation(this.dataItem(e.item.index()).value);
        },
        dataBound: adjustDropDownWidth
    });

    $("#searchVendor").kendoComboBox({
        dataTextField: "label",
        dataValueField: "value",
        filter: "contains",
        autoBind: true,
        autoSync: true,
        delay: 500,
        suggest: true,
        dataSource:
        {
            serverFiltering: true,
            type: "json",
            transport: {
                read: { url: "/api/VendorsAPI", cache: false },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return { term: document.activeElement.value }       //-   Use for combobox
                    }
                }
            }
        },
        select: function(e){
            PopulateVendorInformation(this.dataItem(e.item.index()).value);
        },
        dataBound: adjustDropDownWidth
    });

    function adjustDropDownWidth(e) {
        var listContainer = e.sender.list.closest(".k-list-container");
        listContainer.width(listContainer.width() + kendo.support.scrollbar());
        $(listContainer).css("font-size", "9px");
    }

    function PopulatePropertyInformation(propertyID) {
        $.ajax({
            url: "api/PropertiesAPI",
            type: "Get",
            data: { "propertyID": propertyID },
            contentType: 'application/json; charset=utf-8',
            success: function (data) {

            },
            error: function () {
                $(element).empty().html("---");
            }
        });
    }

    function PopulateVendorInformation(vendorID) {

    }
});


