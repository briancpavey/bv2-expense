﻿$(document).ready(function () {
    var _current_menu_item = getActiveMenuItem("menu-home");

    $(".menu-item").each(function (item) {
        $(this).click(function () {
            setActiveMenuItem(this);
        });
        if ($(this).attr("id") == _current_menu_item) $(this).addClass("active");
    });

    function setActiveMenuItem(item) {
        $("#" + _current_menu_item).removeClass("active");
        localStorage.setItem("menu-item", $(item).attr("id"));
    }

    function getActiveMenuItem(defaultValue) {
        var m = localStorage.getItem("menu-item");
        if (m == null || m.toString().length == 0) m = defaultValue;
        return m
    }
});

