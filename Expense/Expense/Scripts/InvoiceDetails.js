﻿$(document).ready(function () {
    var _currentStatusID = $("#StatusID").val();
    var _next = false;

    $("#invoiceReject").click(function (e) {
        e.preventDefault();
        _next = false;
        $("#dialog-confirm-reject").dialog("open");
    });
    $("#invoicePost").click(function (e) {
        e.preventDefault();
        _next = false;
        $("#dialog-confirm-post").dialog("open");
    });
    $("#invoiceRejectNext").click(function (e) {
        e.preventDefault();
        _next = true;
        $("#dialog-confirm-reject").dialog("open");
    });
    $("#invoicePostNext").click(function (e) {
        e.preventDefault();
        _next = true;
        $("#dialog-confirm-post").dialog("open");
    });

    $("#dialog-message").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-reject").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Yes": function () {
                var guidA = $("#InvoiceGuidA").val();
                var guidB = $("#InvoiceGuidB").val();
                var reason = $("#text-reject").val();
                
                $.post("/Invoices/Reject", { "guidA": guidA, "guidB": guidB, "reason" : reason, "next" : _next }, function (d) {
                    if (d.passed == true) {
                        $("#dialog-message-text").empty().html(d.message);
                        $("#dialog-message").dialog({
                            title: "Invoice Rejected",
                            close: function (event, ui) {
                                if (d.id == null || d.id.length == 0 || d.id == "0")
                                    window.location.href = '/Invoices?status=' + _currentStatusID;
                                else 
                                    window.location.href = '/Invoices/Details/' + d.id;
                            }
                        }).dialog("open");
                    }
                })

                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-post").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Yes": function () {
                var guidA = $("#InvoiceGuidA").val();
                var guidB = $("#InvoiceGuidB").val();
                $.post("/Invoices/Post", { "guidA": guidA, "guidB": guidB, "next": _next }, function (d) {
                    if (d.passed == true) {
                        $("#dialog-message-text").empty().html(d.message);
                        $("#dialog-message").dialog({
                            title: "Invoice Approved",
                            close: function (event, ui) {
                                if (d.id == null || d.id.length == 0 || d.id == "0")
                                    window.location.href = '/Invoices?status=' + _currentStatusID;
                                else
                                    window.location.href = '/Invoices/Details/' + d.id;
                            }
                        }).dialog("open");
                    }
                })

                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#image-preview").elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
    });

    var _image_container = null;
    var _image_text_container = null;
    var _image_queue = {};
    var _image_queue_index = 0;

    function InitializeImageStrip() {
        _image_queue = $("#image-queue").val().split(",");

        if (_image_queue.length < 2) return;

        //  The image container for the zoom
        _image_container = $("#image-preview");
        _image_text_container = $("#image-text");

        if ($("#image-prev") != null) {
            $("#image-prev").click(function () {
                if (_image_queue_index == 0) {
                    _image_queue_index = _image_queue.length - 1;
                } else _image_queue_index -= 1;
                SwapImage(_image_queue_index);
                return false;
            });
        }
        if ($("#image-next") != null) {
            $("#image-next").click(function () {
                if (_image_queue_index == _image_queue.length - 1) {
                    _image_queue_index = 0;
                } else _image_queue_index += 1;
                SwapImage(_image_queue_index);
                return false;
            });
        }

        $(_image_text_container).empty().text("1 of " + _image_queue.length.toString());
    }

    function SwapImage(i) {
        var src = "../../InvoiceImage.ashx?aid=" + _image_queue[i];
        $(_image_container).data('elevateZoom').swaptheimage(src, src);

        $(_image_text_container).empty().text((i + 1).toString() + " of " + _image_queue.length.toString());
    }

    InitializeImageStrip();
});


