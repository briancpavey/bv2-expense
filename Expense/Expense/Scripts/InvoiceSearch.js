﻿$(document).ready(function () {
    $("#button_search").click(function () {
        RefreshInvoiceGrid();
    });

    $("#button_clear").click(function () {
        $("#searchProperty").data("kendoComboBox").value("");
        $("#searchVendor").data("kendoComboBox").value("");
        $("#searchInvoiceDate").val("");
        $("#searchInvoiceNumber").val("");
        RefreshInvoiceGrid();
    });

    $("#searchInvoiceDate").kendoDatePicker({
        // defines the start view
        start: "day",

        // defines when the calendar should return date
        depth: "year",

        // display month and year in the input
        format: "MM/dd/yyyy"
    });

    $("#searchProperty").kendoComboBox({
        dataTextField: "label",
        dataValueField: "value",
        filter: "contains",
        autoBind: true,
        autoSync: true,
        delay: 500,
        suggest: true,
        dataSource:
        {
            serverFiltering: true,
            type: "json",
            transport: {
                read: { url: "/api/PropertiesAPI", cache: false },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        //return { term: data.filter.filters[0].value }     //-   Use for autocomplete
                        return { term: document.activeElement.value }       //-   Use for combobox
                    }
                }
            }
        },
        dataBound: adjustDropDownWidth
    });

    function adjustDropDownWidth(e) {
        var listContainer = e.sender.list.closest(".k-list-container");
        listContainer.width(listContainer.width() + kendo.support.scrollbar());
        $(listContainer).css("font-size", "9px");
    }

    $("#searchVendor").kendoComboBox({
        dataTextField: "label",
        dataValueField: "value",
        filter: "contains",
        autoBind: true,
        autoSync: true,
        delay: 500,
        suggest: true,
        dataSource:
        {
            serverFiltering: true,
            type: "json",
            transport: {
                read: { url: "/api/VendorsAPI", cache: false },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        //return { term: data.filter.filters[0].value }     //-   Use for autocomplete
                        return { term: document.activeElement.value }       //-   Use for combobox
                    }
                }
            }
        },
        dataBound: adjustDropDownWidth
    });
    
    function RefreshInvoiceGrid() {
        var status = GetElementValue("status", "-99");
        var propertyID = GetElementValue("searchProperty", "0");
        var vendorID = GetElementValue("searchVendor", "0");
        var invoiceNumber = GetElementValue("searchInvoiceNumber", "");
        var postPeriodMonth = GetElementValue("searchPostMonth", "0");
        var invoiceDate = GetElementValue("searchInvoiceDate", "");
        var term = GetElementValue("searchTerm", "");

        $("#searchInvoices").kendoGrid({
            dataSource: {
                autoBind: false,
                type: "json",
                serverPaging: true,
                serverSorting: true,
                pageSize: 10,
                transport: {
                    read: {
                        url: "/api/InvoicesAPI",
                        dataType: "json",
                        data: {
                            "status": status,
                            "propertyID": propertyID,
                            "vendorID": vendorID,
                            "invoiceNumber": invoiceNumber,
                            "postPeriodMonth": postPeriodMonth,
                            "invoiceDate": invoiceDate,
                            "term" : term
                        }, 
                    }
                },
                schema: {  total: "total", data: "data" },
            },
            sortable: {
                mode: "single",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 4
            },
            columns: [
            {
                field: "InvoiceNumber",
                title: "Invoice #",
                width: 105,
            },
            {
                field: "InvoiceDate",
                title: "Invoice Date",
                width: 115,
                template: "#= kendo.toString(kendo.parseDate(InvoiceDate, 'yyyy-MM-dd'), 'MM/dd/yyyy')#"
            },
            {
                field: "VendorName",
                title: "Vendor Name",
                template: "#=VendorName# (#=VendorCode#)"
            },
            {
                field: "PropertyName",
                title: "Property Name",
                template: "#=PropertyName# (#=PropertyCode#)"
            },
            {
                field: "NetTotal",
                title: "Total",
                width: 100,
                format: "{0:c2}",
                attributes:{style:"text-align:right;"}
            },
            {
                command: [
                    {
                        name: "Details",
                        click: function (e) {
                            var invoiceID = this.dataItem($(e.currentTarget).closest("tr")).InvoiceID;
                            window.location.href = '/Invoices/Details/' + invoiceID;
                        }
                    }
                ],
                width: 100
            }
            ],
        });
    }

    function GetElementValue(elementid, defaultValue) {
        if ($("#" + elementid) == null || $("#" + elementid).length == 0) return defaultValue;
        else return $("#" + elementid).val();
    }

    RefreshInvoiceGrid();
})