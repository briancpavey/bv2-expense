﻿$(document).ready(function () {
    function RefreshCustomerGrid() {

        $("#gridCustomers").kendoGrid({
            dataSource: {
                autoBind: false,
                type: "json",
                serverPaging: true,
                serverSorting: true,
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/customersapi",
                        dataType: "json",
                    }
                },
                schema: { total: "total", data: "data" },
            },
            sortable: {
                mode: "single",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 4
            },
            columns: [
            {
                field: "Code",
                title: "Code",
                width: 105,
            },
            {
                field: "Name",
                title: "Name",
            },
            {
                field: "Token",
                title: "Token",
            },
            {
                field: "Active",
                title: "Active",
            },
            {
                command: [
                    {
                        name: "Edit",
                        click: function (e) {
                            var customerID = this.dataItem($(e.currentTarget).closest("tr")).CustomerID;
                            window.location.href = '/Customers/Edit/' + customerID;
                        }
                    }
                ],
                width: 100
            }
            ],
        });
    }

    RefreshCustomerGrid();
});