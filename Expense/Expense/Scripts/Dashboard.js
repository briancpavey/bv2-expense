﻿$(document).ready(function () {
    InitializeWorkflow();
    IntializePostPeriodStats();

    UpdateInvoiceActivity("activity-invoice", 0, 0);
    UpdateSystemActivity("activity-system", 9, 0);
});

function InitializeWorkflow() {
    $.ajax({
        url: "api/workflowapi",
        type: "Get",
        data: { "moduleID": 1 },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var workflow = "";
            $.each(data, function (index, item) {
                workflow += "<div class='col-md-2'>" +
                                "<div class='metric panel'>" +
                                    "<span class='icon'><i class='" + item.Css + "'></i></span>" +
                                    "<p>" +
                                        "<span class='number workflow-stat' id='stat-" + item.Status.toString() + "' data-value='" + item.Status.toString() + "'><span class='glyphicon glyphicon-refresh spinning'></span></span>" +
                                        "<span class='title'><a href='/Invoices?status=" + item.Status.toString() + "' class=''>" + item.Name + "</a></span>" +
                                    "</p>" +
                                 "</div>" +
                            "</div>";

            });
            $("#workflow-stats").empty().html(workflow);

            InitializeWorkflowStats();
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function InitializeWorkflowStats() {
    $(".workflow-stat").each(function (e) {
        var id = $(this).attr('id');
        var status = $(this).attr("data-value");

        setTimeout(function () {
            UpdateStat(id, status);
        }, 100);
    });
}

function ElementLoading(elementId) {
    $(element).empty().html('<span class="glyphicon glyphicon-refresh spinning"></span>')
}

function IntializePostPeriodStats() {
    $.ajax({
        url: "api/settingsapi",
        type: "Get",
        data: { "category": "Expense", "key" : "PostPeriod" },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#period-header").text($("#period-header").text() + " - " + data);

            UpdatePeriodStat("period-count", data, 0);
            UpdatePeriodStat("period-paid", data, 1);
            UpdatePeriodStat("period-void", data, 2);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function UpdateStat(elementId, statusId) {
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/dashboardapi",
        type: "Get",
        data: { "status": statusId },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $(element).empty().html(data);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function UpdatePeriodStat(elementId, period, typeid) {
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/dashboardapi",
        type: "Get",
        data: { "period": period, "type" : typeid },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $(element).empty().html(data);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function UpdateInvoiceActivity(elementId, sourceId, typeId)
{
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/auditlogapi",
        type: "Get",
        data: { "sourceId": sourceId, "typeId": typeId },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var rows = "";
            $.each(data, function (index, item) {
                rows += "<tr>" +
                            "<td><a href='/Invoices/Details/" + item.InvoiceID + "'>" + item.InvoiceNumber + "</a></td>" +
                            "<td><a href='mailto:"  + item.Source + "?subject=Invoice%20" + item.InvoiceNumber + "'>" + item.Source + "</a></td>" +
                            "<td align='right'>" + "$" + item.NetTotal.toFixed(2).toString() + "</td>" +
                            "<td>" + item.DateTimeCreated.toString("MM/dd/yyyy").substring(0,19).replace('T',' ') + "</td>" +
                            "<td><span class='label label-" + item.StatusName + "'>" + item.StatusName + "</span></td>" +
                        "</tr>";
            });
            $(element).empty().html(rows);
        },
        error: function (a,b,c) {
            $(element).empty().html("---");
        }
    });
}

function UpdateSystemActivity(elementId, sourceId, typeId) {
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/auditlogapi",
        type: "Get",
        data: { "pageSize": 10, "skip" : 0, "sourceId": sourceId, "typeId": typeId },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var rows = "";
            $.each(data.data, function (index, item) {
                rows += "<tr>" +
                            "<td>" + item.DateTimeCreated.toString("MM/dd/yyyy").substring(0, 19).replace('T', ' ') + "</td>" +
                            "<td>" + item.Source + "</td>" +
                            "<td>" + item.TypeName + "</td>" +
                            "<td>" + item.Status + "</td>" + 
                     "</tr>";
            });
            $(element).empty().html(rows);
        },
        error: function (a, b, c) {
            $(element).empty().html("---");
        }
    });
}