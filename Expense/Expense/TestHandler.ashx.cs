﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Broadvine.DAL;
using Broadvine.Models.Expense;

namespace Expense
{
    /// <summary>
    /// Summary description for TestHandler
    /// </summary>
    public class TestHandler : IHttpHandler
    {
        private System.Text.StringBuilder sb = new System.Text.StringBuilder();

        private void Log(string text)
        {
            sb.AppendLine(text);
        }
        public void ProcessRequest(HttpContext context)
        {
            Log("ProcessRequest Image");

            //Core.AddAuditEntry(null, AuditSource.System, AuditType.Error, "Image Processing", "", null, null, null, "Image Processing");

            if (context.Request["id"] == null) return;

            Log("ProcessRequest Image ID - " + context.Request["Id"].ToString());
            string rootFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["RootStorage"];

            int invoiceID = int.Parse(context.Request["id"].ToString());
            try
            {
                using (BroadvineContext expenseContext = new BroadvineContext())
                {
                    Attachment attachment = expenseContext.Attachments.FirstOrDefault(i => i.InvoiceID == invoiceID);
                    if (attachment != null)
                    {
                        byte[] buffer = GetFileBytes(string.Format("{0}{1}", rootFolder, attachment.FileLocation));
                        Log("Buffer Length is " + buffer.Length);
                        //context.Response.ContentType = "image/jpeg";
                        //context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                        //context.Response.End();
                    } else
                    {
                        Log("Attachment is null!");
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex.Message);
                //context.Response.Clear();
                //context.Response.Write(ex.Message);
                //context.Response.End();
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(sb.ToString().Replace(Environment.NewLine, "<br/>"));

        }

        private byte[] GetFileBytes(string folderFilename)
        {
            byte[] buffer = new byte[] { 0x00 };
            if (System.IO.File.Exists(folderFilename)) buffer = System.IO.File.ReadAllBytes(folderFilename);
            return buffer;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        //public void ProcessRequest(HttpContext context)
        //{
        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();

        //    string rootFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["RootStorage"] + @"201701\0000039\PAGE_1.jpg";

        //    try
        //    {
        //        sb.AppendLine(string.Format("Openning - {0}", rootFolder));
        //        byte[] buffer = new byte[] { 0x00 };
        //        if (System.IO.File.Exists(rootFolder)) buffer = System.IO.File.ReadAllBytes(rootFolder);
        //        sb.AppendLine(string.Format("Buffer length - {0}", buffer.Length));
        //    }
        //    catch (Exception ex)
        //    {
        //        sb.AppendLine(ex.Message);
        //    }

        //    context.Response.ContentType = "text/plain";
        //    context.Response.Write(sb.ToString().Replace(Environment.NewLine, "<br/>"));
        //}

        //public bool IsReusable
        //{
        //    get
        //    {
        //        return false;
        //    }
        //}
    }
}