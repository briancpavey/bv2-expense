﻿using System.Web.Mvc;

namespace Expense.Common.Authentication
{
    public class CustomerFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext context)
        {
            if (context.HttpContext.User.Identity != null && context.HttpContext.User.Identity.IsAuthenticated)
            {
                if (Broadvine.Common.Customer.Current == null)
                {
                    Broadvine.Membership.ApplicationUser user = ApplicationUsers.GetUser(context.HttpContext.User.Identity);
                    Broadvine.Models.Common.Customer customer = ApplicationUsers.GetCustomer(user.CustomerID);
                    Broadvine.Common.Customer.Current = customer;
                }
            }
        }
    }
}
