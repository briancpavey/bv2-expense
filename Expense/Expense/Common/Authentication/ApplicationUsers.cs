﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Broadvine.Membership;
using Broadvine.Models.Expense;
using Broadvine.Common.Helpers;

namespace Expense.Common.Authentication
{
    public class ApplicationUsers
    {
        public static string[] Roles = new string[] { "Sysop", "Administrator", "User" };

        public static bool CreateRoles(string[] roleNames)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            foreach (string role in roleNames)
                if (!roleManager.RoleExists(role))
                    roleManager.Create(new IdentityRole(role));

            return true;
        }

        public static bool CreateUser(ApplicationUser user)
        {
            return CreateUser(user.FirstName, user.LastName, user.Email, user.PhoneNumber, "", new string[] { "User" });
        }

        public static bool CreateUser(string firstName, string lastName, string emailAddress, string userPhoneNumber, string password, string[] roleNames)
        {
            bool result = true;
            try
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var user = userManager.Users.Where(u => u.UserName.Equals(emailAddress));

                if (user.Count() == 0)
                {
                    ApplicationUser u = new ApplicationUser();
                    u.FirstName = firstName;
                    u.LastName = lastName;
                    u.UserName = emailAddress;
                    u.Email = emailAddress;
                    u.EmailConfirmed = true;
                    u.LockoutEnabled = false;
                    u.PhoneNumber = userPhoneNumber;
                    u.PhoneNumberConfirmed = true;
                    IdentityResult r = userManager.Create(u, string.IsNullOrEmpty(password) ? "******" : password);

                    if (r != null && r.Errors.Count() == 0)
                    {
                        foreach (string role in roleNames) userManager.AddToRole(u.Id, role);

                        ResetUserPassword(emailAddress, password:password);

                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            
            return result;
        }

        public static void ResetUserPassword(string userName, bool sendemail = true, string password = "")
        {
            try
            {
                if (string.IsNullOrEmpty(password)) password = System.Web.Security.Membership.GeneratePassword(12, 2);

                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var user = userManager.Users.Where(u => u.UserName.Equals(userName)).FirstOrDefault();

                var provider = new DpapiDataProtectionProvider("Broadvine");
                userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("ASP.NET Identity"));

                var code = userManager.GeneratePasswordResetToken(user.Id);
                var result = userManager.ResetPasswordAsync(user.Id, code, password);

                user.Password = password;
                Common.Email.Emails.SendEmail("Broadvine Login Credentials", Common.Email.Emails.EmailBody("~/Content/Email/PasswordReset.txt", user), new string[] { userName }, null);
            }
            catch (Exception e)
            {
                Core.AddAuditEntry(null, AuditSource.System, AuditType.Error, "ResetUserPassword", e.Message, null, null, null);
                throw e;
            }
        }
        
        public static List<CustomerRole> GetAllCustomerRoles(bool isSySop = false)
        {
            var highestRole = Roles[0];
            var roles = new ApplicationDbContext().CustomerRoles
                            .Where(r => ((r.CustomerID ?? 0) == 0 || r.CustomerID == Broadvine.Common.Customer.Current.CustomerID))
                            .Where(r => isSySop ? true : r.Name != highestRole)
                            .ToList();

            return roles;
        }

        public static void RemoveAllRoles(string userId)
        {
            new ApplicationDbContext().Database.ExecuteSqlCommand(string.Format("DELETE FROM [aspnetuserroles] WHERE UserID = '{0}'", userId.ToString()));
        }

        public static void AddToRoles(string userid, string[] roleIds)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roles = context.CustomerRoles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToArray<string>();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            userManager.AddToRoles(userid, roles);
        }

        public static void CreateRole(string name)
        {
            new ApplicationDbContext().Database.ExecuteSqlCommand(string.Format("INSERT INTO [aspnetroles] ([ID],[Name],[CustomerID]) VALUES ('{0}','{1}',{2})",
                                                                                    Guid.NewGuid().ToString(),
                                                                                    name.Replace("'", "''"),
                                                                                    Broadvine.Common.Customer.Current.CustomerID));

        }
        public static ApplicationUser GetUser(System.Security.Principal.IIdentity identity)
        {
            return new ApplicationDbContext().Users.FirstOrDefault(u => u.Email == identity.Name);
        }

        public static Broadvine.Models.Common.Customer GetCustomer(int? customerID)
        {
            return new Broadvine.DAL.BroadvineContext().Customers.FirstOrDefault(c => c.CustomerID == customerID);
        }
    }
}