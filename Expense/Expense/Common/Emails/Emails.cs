﻿using System;
using System.Linq;
using System.Web;
using System.Net.Mail;
using Broadvine.Models.Expense;
using Broadvine.Common.Helpers;
using Broadvine.Membership;
using System.Reflection;

namespace Expense.Common.Email
{
    public class Emails
    {
        public static string From = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailFrom"].ToString();
        public static string Server = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailServer"].ToString();
        public static string User = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailUserName"].ToString();
        private static string Password = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailPassword"].ToString();
        public static string[] CSS = new string[] { "~/Content/email.css" };

        public static string EmailBody(string templateFolderFilename, ApplicationUser user)
        {
            string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(templateFolderFilename));
            
            foreach (PropertyInfo p in user.GetType().GetProperties())
            {
                try
                {
                    body = body.Replace(string.Format("[%{0}%]", p.Name), user.GetType().GetProperty(p.Name).GetValue(user).ToString());
                }
                catch { }
            }

            return body.Replace(Environment.NewLine, "<br/>");
        }

        public static void SendEmail(string subject, string body, string[] to, string[] bcc)
        {
            try
            {
                if (CSS.Length > 0)
                {
                    string cssScript = "";

                    foreach (string cssFile in CSS) cssScript += System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(cssFile));

                    body = "<html>" +
                                string.Format("<head><style type='text/css'>{0}</style></head>", cssScript) +
                                string.Format("<body style='margin:50px!important;'>{0}</body>", body) +
                            "</html>";
                }

                using (MailMessage message = new MailMessage())
                {
                    if (to.Length > 0)
                        foreach (string emailAddress in to)
                            if (!string.IsNullOrEmpty(emailAddress)) message.To.Add(new MailAddress(emailAddress));

                    if (message.To.Count() == 0) throw new Exception("No email address supplied");

                    message.From = new MailAddress(From);
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient(Server);
                    smtp.EnableSsl = false;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(User, Password);
                    smtp.Port = 25;
                    smtp.Send(message);
                }
            }
            catch (Exception e)
            {
                Core.AddAuditEntry(null, AuditSource.System, AuditType.Error, "SendEmail", e.Message, null, null, null);
            }
        }
    }
}