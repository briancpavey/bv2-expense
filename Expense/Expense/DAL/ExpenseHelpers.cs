﻿using System;
using System.Data.Entity;
using Expense.Models;

namespace Expense.DAL
{
    public class ExpenseHelpers
    {
        public static string RootStorage = System.Web.Configuration.WebConfigurationManager.AppSettings["RootStorage"].ToString();
        public static void AddAuditEntry(BroadvineContext context, AuditSource sourceType, AuditType auditType, string information, string details, int? invoiceID, int? vendorID, int? propertyID, string source = "System")
        {
            if (context == null) context = new BroadvineContext();
            AuditLog entry = new AuditLog()
            {
                DateTimeCreated = DateTime.Now,
                AuditSourceID = sourceType,
                AuditTypeID = auditType,
                Information = information,
                Details = details,
                InvoiceID = invoiceID, 
                VendorID = vendorID,
                PropertyID = propertyID,
                Source = source
            };
            context.AuditLog.Add(entry);
            context.SaveChanges();
        }
    }
}