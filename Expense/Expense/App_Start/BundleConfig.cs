﻿using System.Web;
using System.Web.Optimization;

namespace Expense
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            "~/Scripts/jquery-ui.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css",
                      "~/Content/bootstrap.css",
                      "~/Content/icon-sets.css",
                      "~/Content/main.min.css",
                      "~/Content/jquery-ui.min.css",
                      "~/Content/invoicecompare.css",
                      "~/Content/Kendo/kendo.common-bootstrap.min.css",
                      "~/Content/Kendo/kendo.common.min.css",
                      "~/Content/Kendo/kendo.default.min.css",
                      "~/Content/Kendo/kendo.default.mobile.min.css"));

            //  Misc
            bundles.Add(new ScriptBundle("~/bundles/misc").Include(
                      "~/Scripts/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                      "~/Scripts/plugins/jquery-easypiechart/jquery.easypiechart.min.js",
                      "~/Scripts/plugins/chartist/chartist.min.js",
                      "~/Scripts/klorofil.min.js",
                      "~/Scripts/jquery.elevateZoom-3.0.8.min.js",
                      "~/Scripts/menus.js"));
            //  Kendo
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                      "~/Scripts/Kendo/kendo.all.min.js"));
        }
    }
}
