﻿$(document).ready(function () {
    var _currentStatusID = $("#StatusID").val();

    $("#invoiceReject").click(function (e) {
        e.preventDefault();
        $("#dialog-confirm-reject").dialog("open");
    });
    $("#invoicePost").click(function (e) {
        e.preventDefault();
        $("#dialog-confirm-post").dialog("open");
    });

    $("#dialog-message").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-reject").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Yes": function () {
                var guidA = $("#InvoiceGuidA").val();
                var guidB = $("#InvoiceGuidB").val();
                var reason = $("#text-reject").val();

                $.post("/Invoices/Reject", { "guidA": guidA, "guidB": guidB, "reason" : reason }, function (d) {
                    if (d.passed == true) {
                        $("#dialog-message-text").empty().html(d.message);
                        $("#dialog-message").dialog({
                            title: "Invoice Rejected",
                            close: function (event, ui) {
                                window.location.href = '/Invoices?status=' + _currentStatusID;
                            }
                        }).dialog("open");
                    }
                })

                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-post").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Yes": function () {
                var guidA = $("#InvoiceGuidA").val();
                var guidB = $("#InvoiceGuidB").val();
                $.post("/Invoices/Post", { "guidA": guidA, "guidB": guidB }, function (d) {
                    if (d.passed == true) {
                        $("#dialog-message-text").empty().html(d.message);
                        $("#dialog-message").dialog({
                            title: "Invoice Approved",
                            close: function (event, ui) {
                                window.location.href = '/Invoices?status=' + _currentStatusID;
                            }
                        }).dialog("open");
                    }
                })

                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#image-preview").elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
    });

});
