﻿$(document).ready(function () {
    IntializePostPeriodStats();

    UpdateStat("stat-pending", 0);
    UpdateStat("stat-post", 2);
    UpdateStat("stat-posted", 3);
    UpdateStat("stat-paid", 4);
    UpdateStat("stat-void", 9);
    UpdateStat("stat-rejected", -1);

    UpdateActivity("activity-invoice", 0, 0);
});

function ElementLoading(elementId) {
    $(element).empty().html('<span class="glyphicon glyphicon-refresh spinning"></span>')
    //
}

function IntializePostPeriodStats() {
    $.ajax({
        url: "api/settingsapi",
        type: "Get",
        data: { "category": "Expense", "key" : "PostPeriod" },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#period-header").text($("#period-header").text() + " - " + data);

            UpdatePeriodStat("period-count", data, 0);
            UpdatePeriodStat("period-paid", data, 1);
            UpdatePeriodStat("period-void", data, 2);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function UpdateStat(elementId, statusId) {
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/dashboardapi",
        type: "Get",
        data: { "status": statusId },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $(element).empty().html(data);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function UpdatePeriodStat(elementId, period, typeid) {
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/dashboardapi",
        type: "Get",
        data: { "period": period, "type" : typeid },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $(element).empty().html(data);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}

function UpdateActivity(elementId, sourceId, typeId)
{
    var element = $("#" + elementId);
    if (element == null || element.length == 0) return;

    $.ajax({
        url: "api/auditlogapi",
        type: "Get",
        data: { "sourceId": sourceId, "typeId": typeId },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var rows = "";
            $.each(data, function (index, item) {
                rows += "<tr>" +
                            "<td><a href='/Invoices/Details/" + item.Invoice.InvoiceID + "'>" + item.Invoice.InvoiceNumber + "</a></td>" +
                            "<td><a href='mailto:"  + item.Source + "?subject=Invoice%20" + item.Invoice.InvoiceNumber + "'>" + item.Source + "</a></td>" +
                            "<td>" + item.Invoice.NetTotal + "</td>" +
                            "<td>" + item.DateTimeCreated + "</td>" +
                            "<td><span class='label label-" + item.Invoice.StatusName + "'>" + item.Invoice.StatusName + "</span></td>" +
                        "</tr>";
            });
            $(element).empty().html(rows);
        },
        error: function () {
            $(element).empty().html("---");
        }
    });
}
