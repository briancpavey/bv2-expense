﻿$(document).ready(function () {
    $("#button_search").click(function () {
        RefreshInvoiceGrid();
    });

    $("#searchInvoiceDate").kendoDatePicker({
        // defines the start view
        start: "day",

        // defines when the calendar should return date
        depth: "year",

        // display month and year in the input
        format: "MM/dd/yyyy"
    });

    $("#searchProperty").kendoComboBox({
        dataTextField: "NameWithCode",
        dataValueField: "PropertyID",
        Filter: "contains",
        autoBind: true,
        delay:500,
        dataSource:
        {
            serverFiltering: true,
            type: "json",
            transport: {
                read: { url: "/api/PropertiesAPI", cache: false },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        //return { term: data.filter.filters[0].value }     //-   Use for autocomplete
                        return { term: document.activeElement.value }       //-   Use for combobox
                    }
                }
            }
        }
    });

    $("#searchVendor").kendoComboBox({
        dataTextField: "NameWithCode",
        dataValueField: "VendorID",
        Filter: "contains",
        autoBind: true,
        delay:500,
        dataSource:
        {
            serverFiltering: true,
            type: "json",
            transport: {
                read: { url: "/api/VendorsAPI", cache: false },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        //return { term: data.filter.filters[0].value }     //-   Use for autocomplete
                        return { term: document.activeElement.value }       //-   Use for combobox
                    }
                }
            }
        }
    });

    $("#searchInvoices").kendoGrid({
        groupable: false,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [
            {
                field: "InvoiceNumber",
                title: "Invoice #",
                width: 105,
            },
            {
                field: "InvoiceDate",
                title: "Invoice Date",
                width: 115,
                template: "#= kendo.toString(kendo.parseDate(InvoiceDate, 'yyyy-MM-dd'), 'MM/dd/yyyy')#"
            },
            {
                field: "Vendor.NameWithCode",
                title: "Vendor Name"
            },
            {
                field: "Property.NameWithCode",
                title: "Property Name"
            },
            {
                field: "NetTotal",
                title: "Total",
                width: 70,
            },
            {
                command: [
                    {
                        name: "Details",
                        click: function (e) {
                            var invoiceID = this.dataItem($(e.currentTarget).closest("tr")).InvoiceID;
                            window.location.href = '/Invoices/Details/' + invoiceID;
                        }
                    }
                ],
                width:100
            }
        ],
        selectable: "single",
        change: onChange
    });

    function onChange(arg) {
        //var selected = $.map(this.select(), function (item) {

        //    alert($(item).text());
        //    return $(item).text();
        //});

        //kendoConsole.log("Selected: " + selected.length + " item(s), [" + selected.join(", ") + "]");
    }

    function RefreshInvoiceGrid() {
        var form = $("form").serialize();
        var status = $("#status").val();
        var propertyID = $("#searchProperty").val();
        var vendorID = $("#searchVendor").val();
        var invoiceNumber = $("#searchInvoiceNumber").val();
        var postPeriodMonth = $("#searchPostMonth").val();
        var invoiceDate = $("#searchInvoiceDate").val()
        $.ajax({
            url: "/api/InvoicesAPI",
            data: { "status": status, "propertyID": propertyID, "vendorID": vendorID, "invoiceNumber": invoiceNumber, "postPeriodMonth": postPeriodMonth, "invoiceDate" : invoiceDate },
            dataType: 'json',
            success: function (data) {
                var grid = $('#searchInvoices').getKendoGrid();
                grid.dataSource.data(data);
                grid.refresh();
            }
        });
    }

    RefreshInvoiceGrid();
})