﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common.Helpers;

namespace Expense
{
    public class InvoiceImage : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int invoiceID = 0;
            int attachmentID = 0;

            if (context.Request["id"] != null) invoiceID = int.Parse(context.Request["id"].ToString());
            if (context.Request["aid"] != null) attachmentID = int.Parse(context.Request["aid"].ToString());
            if (invoiceID == 0 && attachmentID == 0) return;

            string rootFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["RootStorage"];

            try
            {
                using (BroadvineContext expenseContext = new BroadvineContext())
                {
                    Attachment attachment = expenseContext.Attachments.FirstOrDefault(i => (attachmentID == 0) ? i.InvoiceID == invoiceID : i.AttachmentID == attachmentID);
                    if (attachment != null)
                    {
                        byte[] buffer = GetFileBytes(string.Format("{0}{1}", rootFolder, attachment.FileLocation));

                        context.Response.ContentType = "image/jpeg";
                        context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                    }
                }
            } catch (Exception ex)
            {
                context.Response.Clear();
                context.Response.Write(ex.Message);
            }

        }

        private byte[] GetFileBytes(string folderFilename)
        {
            byte[] buffer = new byte[] { 0x00 };
            if (System.IO.File.Exists(folderFilename)) buffer = System.IO.File.ReadAllBytes(folderFilename);
            return buffer;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}