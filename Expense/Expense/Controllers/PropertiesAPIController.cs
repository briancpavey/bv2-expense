﻿using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Http;
using Broadvine.DAL;
using Broadvine.Common.Extensions;

namespace Expense.Controllers
{
    public class PropertiesAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        [HttpGet]
        [AllowAnonymous]
        public object Get(string term)
        {
            return Json(
                    context.Properties.Select(p => new
                    {
                        label = p.Name + " (" + p.Code + ")",
                        value = p.PropertyID,
                        CustomerID = p.CustomerID
                    })
                    .Where(p => p.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                    .Where(p => (string.IsNullOrEmpty(term) ? true : (p.label.Contains(term))))
                    .Take(100)
                    .OrderBy(p => p.label)
            );
        }

        public object Get(int pageSize, int skip)
        {
            var total = context.Properties.Where(p => p.CustomerID == Broadvine.Common.Customer.Current.CustomerID).Count();

            NameValueCollection c = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            var sortField = c["sort[0][field]"] ?? "Name";
            var sortDirection = c["sort[0][dir]"] ?? "ASC";
            c.Clear();

            var data = context.Properties
                .Select(p => new
                {
                   PropertyID = p.PropertyID,
                   Code = p.Code,
                   Name = p.Name,
                   Address1 = p.Address1,
                   Address2 = p.Address2,
                   City = p.City,
                   State = p.State,
                   Zip = p.ZipCode, 
                   Phone = p.Phone,
                   InvoiceCount = (context.Invoices.Where(i=> i.PropertyID == p.PropertyID).Count()),
                   CustomerID = p.CustomerID
                })
                .Where(p => p.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                .OrderByField(sortField, string.Compare(sortDirection, "desc", true) != 0)
                .Skip(skip)
                .Take(pageSize)
                .ToList();

            return Json(new { total = total, data = data });
        }

        public object Get(int propertyID)
        {
            return context.Properties.Find(propertyID);
        }
    }
}