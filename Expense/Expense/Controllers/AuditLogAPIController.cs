﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common.Extensions;

namespace Expense.Controllers
{
    public class AuditLogAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        public object Get(int sourceId, int typeId, int limit = 10)
        {
            return context.AuditLog
                .Select(a=> new
                {
                    AuditSourceID = a.AuditSourceID,
                    AuditTypeID = a.AuditTypeID,
                    InvoiceID = a.InvoiceID,
                    InvoiceNumber = a.Invoice.InvoiceNumber,
                    Source = a.Source,
                    NetTotal = a.Invoice.NetTotal,
                    StatusName = a.Invoice.StatusID.ToString(),
                    DateTimeCreated = a.DateTimeCreated,
                    CustomerID = a.CustomerID

                })
                .Where(a => a.AuditSourceID == (AuditSource)sourceId)
                .Where(a => a.AuditTypeID == (AuditType)typeId)
                .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                .OrderByDescending(a => a.DateTimeCreated)
                .Take(limit)
                .ToList();
        }

        public object Get(int pageSize, int skip, int sourceId, int typeId)
        {
            NameValueCollection p = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            var sortField = p["sort[0][field]"] ?? "DateTimeCreated";
            var sortDirection = p["sort[0][dir]"] ?? "DESC";
            p.Clear();

            var total = context.AuditLog
                        .Where(a => (sourceId == -1 ? true : a.AuditSourceID == (AuditSource)sourceId))
                        .Where(a => a.AuditTypeID == (AuditType)typeId)
                        .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                        .Count();

            var data = context.AuditLog
                        .Select(a => new
                        {
                            AuditLogID = a.AuditLogID,
                            AuditSourceID = a.AuditSourceID,
                            AuditTypeID = a.AuditTypeID,
                            DateTimeCreated = a.DateTimeCreated,
                            InvoiceNumber = a.Invoice.InvoiceNumber,
                            PropertyName = a.Property.Name, 
                            Source = a.Source,
                            Status = a.Information,
                            SourceName = a.AuditSourceID.ToString(),
                            TypeName = a.AuditTypeID.ToString(),
                            CustomerID = a.CustomerID
                        })
                        .Where(a => (sourceId == -1 ? true : a.AuditSourceID == (AuditSource)sourceId))
                        .Where(a => a.AuditTypeID == (AuditType)typeId)
                        .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                        .OrderByField(sortField, string.Compare(sortDirection, "desc", true) != 0)
                        .Skip(skip)
                        .Take(pageSize)
                        .ToList();
            return Json(new { total = total, data = data });
        }
    }
}
