﻿using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Http;
using Broadvine.DAL;
using Broadvine.Common.Extensions;

namespace Expense.Controllers
{
    public class CustomersAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        public object Get(int pageSize, int skip)
        {
            var total = context.Customers.Count();

            NameValueCollection c = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            var sortField = c["sort[0][field]"] ?? "Name";
            var sortDirection = c["sort[0][dir]"] ?? "ASC";
            c.Clear();

            var data = context.Customers
                .Select(p => new
                {
                    CustomerID = p.CustomerID,
                    Code = p.Code,
                    Name = p.Name,
                    Token = p.Token,
                    Active = p.Active
                })
                .OrderByField(sortField, string.Compare(sortDirection, "desc", true) != 0)
                .Skip(skip)
                .Take(pageSize)
                .ToList();

            return Json(new { total = total, data = data });
        }
    }
}
