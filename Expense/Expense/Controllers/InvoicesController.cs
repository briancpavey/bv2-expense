﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Globalization;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common.Helpers;

namespace Expense.Controllers
{
    public class InvoicesController : Controller
    {
        private BroadvineContext db = new BroadvineContext();

        // GET: Invoices
        public ActionResult Index()
        {
            //  Invoice status  
            int status = Request["status"] == null ? 0 : int.Parse(Request["status"].ToString());
            var months = new SelectList(Enumerable.Range(1, 12).Select(x => new SelectListItem()
                                                                            {
                                                                                Text = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[x - 1],
                                                                                Value = x.ToString(),
                                                                                Selected = (x == DateTime.Now.Month),
                                                                            }), "Value", "Text");
            
            ViewBag.SearchMonth = months;
            ViewBag.Status = status;
            ViewBag.StatusName = ((InvoiceStatus)status).ToString();

            return View();
        }

        public ActionResult Search(string searchterm)
        {
            return View();
        }

        // GET: Invoices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }

            ViewBag.Status = (int)invoice.StatusID;
            ViewBag.StatusName = invoice.StatusID.ToString();
            ViewBag.AuditLog = db.AuditLog.Where(a => a.InvoiceID == id).ToList();

            if (invoice.StatusID == InvoiceStatus.Pending)
                ViewBag.ShowNext = db.Invoices.Count(i => i.StatusID == InvoiceStatus.Pending && i.InvoiceID != invoice.InvoiceID) > 0;
            else ViewBag.ShowNext = false;

            IEnumerable<int> ids = from a in db.Attachments
                                   where a.InvoiceID == id
                                   select a.AttachmentID;
            ViewBag.AttachmentID = String.Join(",", ids.ToArray<int>());

            Attachment attachment = db.Attachments.FirstOrDefault(a => a.InvoiceID == id);
            if (attachment != null) ViewBag.Image = string.Format("{0}{1}", Core.RootStorage, attachment.FileLocation);
            
            return View(invoice);
        }

        // GET: Invoices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvoiceID,InvoiceGuidA,InvoiceGuidB,PropertyID,StatusID,VendorID,DocumentID,InvoiceNumber,AccountNumber,InvoiceDate,DueDate,PostPeriod,Notes,CheckNumber,CheckDate,SubTotal,TaxTotal,ShippingTotal,NetTotal,BuyerFirstName,BuyerLastName,ReceivedDate,PostedDate,URL,Transmitted,TransmittedDate")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Invoices.Add(invoice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(invoice);
        }

        // GET: Invoices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return View(new Invoice());
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvoiceID,InvoiceGuidA,InvoiceGuidB,PropertyID,StatusID,VendorID,DocumentID,InvoiceNumber,AccountNumber,InvoiceDate,DueDate,PostPeriod,Notes,CheckNumber,CheckDate,SubTotal,TaxTotal,ShippingTotal,NetTotal,BuyerFirstName,BuyerLastName,ReceivedDate,PostedDate,URL,Transmitted,TransmittedDate")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoice);
        }

        // GET: Invoices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Reject")]
        public ActionResult Reject(string guidA, string guidB, string reason, string next)
        {
            bool result = true;
            string message = "Invoice Rejected Successfully";
            int currentPropertyID = 0;
            int nextId = 0;
            InvoiceStatus currentStatus = InvoiceStatus.Pending;
            
            try
            {
                Invoice invoice = db.Invoices.FirstOrDefault(i => i.InvoiceGuidA.ToString() == guidA && i.InvoiceGuidB.ToString() == guidB);
                if (invoice != null)
                {
                    currentPropertyID = invoice.PropertyID;
                    currentStatus = invoice.StatusID ?? InvoiceStatus.Pending;
                    invoice.StatusID = InvoiceStatus.Rejected;
                    invoice.Notes += string.Format("{0}[{1} REJECTED - {2}]{0}{3}", Environment.NewLine, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), HttpContext.User.Identity.Name, reason);
                    invoice.Transmitted = false;
                    invoice.RejectedReason = reason;
                    db.SaveChanges();

                    Core.AddAuditEntry(db, AuditSource.User, AuditType.Info, "Rejected", reason, invoice.InvoiceID, invoice.VendorID, invoice.PropertyID, HttpContext.User.Identity.Name);
                }
            }
            catch (Exception e)
            {
                result = false;
                message = string.Format("An error occurred while rejecting the invoice.");
            }

            if (!string.IsNullOrEmpty(next) && bool.Parse(next))
            {
                var nextInvoice = db.Invoices.FirstOrDefault(i => i.StatusID == currentStatus && i.PropertyID == currentPropertyID);
                if (nextInvoice != null) nextId = nextInvoice.InvoiceID;
            }

            return Json(new { passed = result, message = message, id = nextId });
        }

        [HttpPost, ActionName("Post")]
        public ActionResult Post(string guidA, string guidB, string next)
        {
            bool result = true;
            string message = "Invoice Posted Successfully";
            int currentPropertyID = 0;
            int nextId = 0;
            InvoiceStatus currentStatus = InvoiceStatus.Pending;

            try
            { 
                Invoice invoice = db.Invoices.FirstOrDefault(i => i.InvoiceGuidA.ToString() == guidA && i.InvoiceGuidB.ToString() == guidB);
                if (invoice != null)
                {
                    currentPropertyID = invoice.PropertyID;
                    invoice.StatusID = InvoiceStatus.Post;
                    db.SaveChanges();

                    Core.AddAuditEntry(db, AuditSource.User, AuditType.Info, "Post", "", invoice.InvoiceID, invoice.VendorID, invoice.PropertyID, HttpContext.User.Identity.Name);
                }
            }
            catch (Exception e)
            {
                    result = false;
                    message = string.Format("An error occurred while posting the invoice.");
            }

            if (!string.IsNullOrEmpty(next) && bool.Parse(next))
            {
                var nextInvoice = db.Invoices.FirstOrDefault(i => i.StatusID == currentStatus && i.PropertyID == currentPropertyID);
                if (nextInvoice != null) nextId = nextInvoice.InvoiceID;
            }

            return Json(new { passed = result, message = message, id = nextId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
