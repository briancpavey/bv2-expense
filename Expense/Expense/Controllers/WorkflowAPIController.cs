﻿using System.Web.Http;
using System.Linq;
using Broadvine.DAL;
using Broadvine.Models.Common;
using System.Text;

namespace Expense.Controllers
{
    public class WorkflowAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        public IQueryable Get(int moduleID)
        {
            var menuItems = context.Workflow
                                .Where(f => f.CustomerID == Broadvine.Common.Customer.Current.CustomerID && f.ModuleID == moduleID && f.IsMenuItem == true)
                                .Select(f => new { Name = f.Name, Status = f.StatusID, Css = f.MenuCssClass, SortOrder = f.SortOrder })
                                .OrderBy(f => f.SortOrder);
            return menuItems;
        }
    }
}
