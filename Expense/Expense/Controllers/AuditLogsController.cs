﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Broadvine.DAL;
using Broadvine.Models.Expense;

namespace Expense.Controllers
{
    public class AuditLogsController : Controller
    {
        private BroadvineContext db = new BroadvineContext();

        // GET: AuditLogs
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult System()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
