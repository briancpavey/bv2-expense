﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Http;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common.Extensions;

namespace Expense.Controllers
{
    public class InvoicesAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        public IEnumerable<Invoice> Get(int? status, int? propertyID, int? vendorID, string invoiceNumber, int? postPeriodMonth, string invoiceDate)
        {
            int postPeriod = postPeriodMonth ?? 0;
            if (postPeriod > 0) postPeriod = int.Parse(string.Format("{0}{1}", DateTime.Now.Year, postPeriod.ToString("00")));
            
            postPeriod = 0;

            return context.Invoices
                    .Where(i => i.StatusID == (InvoiceStatus)(status ?? 0))
                    .Where(i => (propertyID > 0 ? i.PropertyID == propertyID : true))
                    .Where(i => (vendorID > 0 ? i.VendorID == vendorID : true))
                    .Where(i => (string.IsNullOrEmpty(invoiceNumber) ? true : i.InvoiceNumber.Contains(invoiceNumber)))
                    .Where(i => (postPeriod > 0 ? i.PostPeriod == postPeriod : true))
                    .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                    .Take(100)
                    .ToList();
        }

        
        public object Get(int pageSize, int skip, string[] sort, int? status, int? propertyID, int? vendorID, string invoiceNumber, int? postPeriodMonth, string invoiceDate, string term)
        {
            int postPeriod = postPeriodMonth ?? 0;
            if (postPeriod > 0) postPeriod = int.Parse(string.Format("{0}{1}", DateTime.Now.Year, postPeriod.ToString("00")));

            DateTime invoiceDateTime; 
            DateTime.TryParse(invoiceDate, out invoiceDateTime);

            postPeriod = 0;
            var total = context.Invoices
                        .Where(i => ((status ?? -99) == -99) ? true : i.StatusID == (InvoiceStatus)(status ?? 0))
                        .Where(i => (propertyID > 0 ? i.PropertyID == propertyID : true))
                        .Where(i => (vendorID > 0 ? i.VendorID == vendorID : true))
                        .Where(i => (string.IsNullOrEmpty(invoiceNumber) ? true : i.InvoiceNumber.Contains(invoiceNumber)))
                        .Where(i => (postPeriod > 0 ? i.PostPeriod == postPeriod : true))
                        .Where(i => (string.IsNullOrEmpty(term) ? true : i.InvoiceNumber.Contains(term)))
                        .Where(i => (invoiceDateTime.Year == 1 ? true : i.InvoiceDate == invoiceDateTime))
                        .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                        .Count();

            try
            {
                NameValueCollection p = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                var sortField = p["sort[0][field]"] ?? "ReceivedDate";
                var sortDirection = p["sort[0][dir]"] ?? "DESC";
                p.Clear();

                var data = context.Invoices
                            .Select(x => new
                            {
                                ReceivedDate = x.ReceivedDate,
                                InvoiceNumber = x.InvoiceNumber,
                                InvoiceDate = x.InvoiceDate,
                                StatusID = x.StatusID,
                                PropertyID = x.PropertyID,
                                VendorID = x.VendorID,
                                PostPeriod = x.PostPeriod,
                                InvoiceID = x.InvoiceID,
                                NetTotal = x.NetTotal,
                                VendorName = x.Vendor.Name,
                                VendorCode = x.Vendor.Code,
                                PropertyName = x.Property.Name,
                                PropertyCode = x.Property.Code,
                                CustomerID = x.CustomerID
                            })
                            .Where(i => ((status ?? -99) == -99) ? true : i.StatusID == (InvoiceStatus)(status ?? 0))
                            .Where(i => (propertyID > 0 ? i.PropertyID == propertyID : true))
                            .Where(i => (vendorID > 0 ? i.VendorID == vendorID : true))
                            .Where(i => (string.IsNullOrEmpty(invoiceNumber) ? true : i.InvoiceNumber.Contains(invoiceNumber)))
                            .Where(i => (postPeriod > 0 ? i.PostPeriod == postPeriod : true))
                            .Where(i => (string.IsNullOrEmpty(term) ? true : i.InvoiceNumber.Contains(term)))
                            .Where(i => (invoiceDateTime.Year == 1 ? true : i.InvoiceDate == invoiceDateTime))
                            .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                            .OrderByField(sortField, string.Compare(sortDirection, "desc", true) != 0)
                            .Skip(skip)
                            .Take(pageSize)
                            .ToList();
                return Json(new { total = total, data = data });
            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public object Get(int pageSize, int skip, string term)
        {
            var total = context.Invoices
                        .Where(i => (string.IsNullOrEmpty(term) ? true : i.InvoiceNumber.Contains(term)));
            try
            {
                NameValueCollection p = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                var sortField = p["sort[0][field]"] ?? "ReceivedDate";
                var sortDirection = p["sort[0][dir]"] ?? "DESC";
                p.Clear();

                var data = context.Invoices
                            .Select(x => new
                            {
                                ReceivedDate = x.ReceivedDate,
                                InvoiceNumber = x.InvoiceNumber,
                                InvoiceDate = x.InvoiceDate,
                                StatusID = x.StatusID,
                                PropertyID = x.PropertyID,
                                VendorID = x.VendorID,
                                PostPeriod = x.PostPeriod,
                                InvoiceID = x.InvoiceID,
                                NetTotal = x.NetTotal,
                                VendorName = x.Vendor.Name,
                                VendorCode = x.Vendor.Code,
                                PropertyName = x.Property.Name,
                                PropertyCode = x.Property.Code,
                                CustomerID = x.CustomerID
                            })
                            .Where(i => (string.IsNullOrEmpty(term) ? true : i.InvoiceNumber.Contains(term)))
                            .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                            .OrderByField(sortField, string.Compare(sortDirection, "desc", true) != 0)
                            .Skip(skip)
                            .Take(pageSize)
                            .ToList();
                return Json(new { total = total, data = data });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}