﻿using System.Web.Http;
using Broadvine.DAL;

namespace Expense.Controllers
{
    public class SettingsAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();
        public string Get(string category, string key)
        {
            return Broadvine.Common.Settings.Setting.GetSetting(category, key, "");
        }
    }
}
