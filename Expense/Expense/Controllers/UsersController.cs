﻿using System;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Broadvine.Membership;
using Broadvine.Models.Expense;
using Broadvine.Common.Helpers;
using System.Linq;

namespace Expense.Controllers
{
    public class UsersController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View(context.Users);
        }

        public ActionResult Details(string id)
        {
            if (id == null)
                id = context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).Id;

            return View(context.Users.Find(id));
        }

        public ActionResult Edit(string id)
        {
            var userID = context.Users.Find(id).UserID.ToString();
            var allRoles = Common.Authentication.ApplicationUsers.GetAllCustomerRoles(User.IsInRole(Common.Authentication.ApplicationUsers.Roles[0]));
            var userRoles = context.UserRoles.Where(r => r.UserId == userID).Select(r => r.RoleId);
            var options = allRoles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id,
                Selected = userRoles.Contains(r.Id)
            });
            
            ViewBag.Roles = options;
            return View(context.Users.Find(id));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ApplicationUser user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Guid guid = Guid.Empty;
                    ApplicationUser u = context.Users.Find(user.Id);
                    if (u != null)
                    {
                        guid = u.UserID;
                        u.FirstName = user.FirstName;
                        u.LastName = user.LastName;
                        u.LockoutEnabled = user.LockoutEnabled;
                        u.PhoneNumber = user.PhoneNumber;

                        context.Entry(u).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                   
                    if (guid != Guid.Empty && Request["UserRoles"] != null)
                    {
                        string[] userRoles = Request["UserRoles"].ToString().Split(',');
                        if (userRoles.Length > 0)
                        {
                            Common.Authentication.ApplicationUsers.RemoveAllRoles(guid.ToString());
                            Common.Authentication.ApplicationUsers.AddToRoles(guid.ToString(), userRoles);
                        }
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    Console.WriteLine(e.ToString());
                    Core.AddAuditEntry(null, AuditSource.System, AuditType.Error, "User Edit", e.Message, null, null, null, HttpContext.User.Identity.Name);
                }

                return RedirectToAction("Index");
            }

            return View(user);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ApplicationUser user)
        {
            if (ModelState.IsValid)
            {
                Common.Authentication.ApplicationUsers.CreateUser(user);

                return RedirectToAction("Index");
            }

            return View(user);
        }

        [HttpPost, ActionName("ResetPassword")]
        public ActionResult ResetPassword(string userId, string userName)
        {
            bool result = true;
            string message = "Password reset successful";

            try
            {
                Common.Authentication.ApplicationUsers.ResetUserPassword(userName, true);
            } catch (Exception e)
            {
                result = false;
                message = "Error occurred while reseting password.";
            }
            
            return Json(new { passed = result, message = message });
        }

        [HttpPost, ActionName("ResetMyPassword")]
        public ActionResult ResetMyPassword(string userId, string userName, string password1, string password2)
        {
            bool result = false;
            string message;

            if (string.IsNullOrEmpty(password1) || string.IsNullOrEmpty(password2)) message = "You must enter a password in both fields.";
            else if (password1 != password2) message = "Passwords do not match";
            else if (password1.Length < 5) message = "Password must contain at least 5 characters";
            else
            {
                try
                {
                    Common.Authentication.ApplicationUsers.ResetUserPassword(userName, false, password1);
                    result = true;
                    message = "Password reset successful";
                }
                catch (Exception e)
                {
                    message = "Error occurred while reseting password.";
                }
            }

            return Json(new { passed = result, message = message });
        }
    }
}