﻿using System.Linq;
using System.Web.Http;
using Broadvine.DAL;

namespace Expense.Controllers
{
    public class VendorsAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        [HttpGet]
        [AllowAnonymous]
        public object Get(string term)
        {
            return Json(
                    context.Vendors.Select(p => new
                    {
                        label = p.Name + " (" + p.Code + ")",
                        value = p.VendorID,
                        CustomerID = p.CustomerID
                    })
                    .Where(p => p.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                    .Where(p => (string.IsNullOrEmpty(term) ? true : (p.label.Contains(term))))
                    .Take(100)
                    .OrderBy(p => p.label)
            );
        }
    }
}