﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Broadvine.DAL;
using Broadvine.Membership;
using Broadvine.Models.Expense;
using Broadvine.Common.Extensions;

namespace Expense.Controllers
{
    public class RolesAPIController : ApiController
    {
        public object Get()
        {
            var data = Common.Authentication.ApplicationUsers.GetAllCustomerRoles();
            return Json(new { total = data.Count(), data = data });
        }
    }
}
