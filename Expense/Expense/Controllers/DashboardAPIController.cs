﻿using System.Web.Http;
using System.Linq;
using Broadvine.DAL;
using Broadvine.Models.Expense;

namespace Expense.Controllers
{
    public class DashboardAPIController : ApiController
    {
        private BroadvineContext context = new BroadvineContext();

        enum StatusType
        {
            InvoiceCount = 0,
            PaidAmount = 1,
            VoidAmount = 2
        }

        public int Get(InvoiceStatus status)
        {
            return context.Invoices
                .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                .Count(i => i.StatusID == status);
        }

        public IQueryable Get(string term)
        {
            return context.Properties
                .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                .Where(p => (string.IsNullOrEmpty(term) ? true : p.Name.Contains(term)))
                .OrderBy(p => p.Name);
        }

        public string Get(int period, int type)
        {
            string value = "0";
            switch ((StatusType)type)
            {
                case StatusType.InvoiceCount:
                    value = context.Invoices
                                    .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                                    .Count(i => i.PostPeriod == period).ToString();
                    break;
                case StatusType.PaidAmount:
                    value = context.Invoices
                        .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                        .Where(i => i.PostPeriod == period && i.StatusID == InvoiceStatus.Paid).Select(i => i.NetTotal).DefaultIfEmpty(0).Sum().ToString("$#,###,##0.00");
                    break;
                case StatusType.VoidAmount:
                    value = context.Invoices
                        .Where(i => i.CustomerID == Broadvine.Common.Customer.Current.CustomerID)
                        .Where(i => i.PostPeriod == period && i.StatusID == InvoiceStatus.Void).Select(i => i.NetTotal).DefaultIfEmpty(0).Sum().ToString("$#,###,##0.00");
                    break;
            }

            return value;
        }

        
    }
}
