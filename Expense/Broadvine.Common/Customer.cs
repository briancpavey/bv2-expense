﻿using System;
using System.Web;
using System.Linq;
namespace Broadvine.Common
{
    public static class Customer
    {
        public static string IdentityName
        {
            get
            {
                if (HttpContext.Current != null) return HttpContext.Current.User.Identity.Name;
                return System.Reflection.Assembly.GetExecutingAssembly().Location;
            }
        }

        public static Broadvine.Models.Common.Customer Current
        {
            get
            {
                return ((Broadvine.Models.Common.Customer)Settings.Setting.GetSetting(IdentityName));
            }
            set {
                if (value != null) Settings.Setting.SetSetting(IdentityName, value);
            }
        }

        public static Broadvine.Models.Common.Customer Get(string token)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(token, out guid);
            return new Broadvine.DAL.BroadvineContext().Customers.FirstOrDefault(c => c.Token == guid);
        }
    }
}
