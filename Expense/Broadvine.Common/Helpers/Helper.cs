﻿using System;
using System.Data.Entity;
using Broadvine.Models.Expense;
using Broadvine.DAL;
using Broadvine.Common.Enums;

namespace Broadvine.Common.Helpers
{
    public class Core
    {
        private static string _rootStorageDirectory = "";
        public static string RootStorage
        {
            get
            {
                if (string.IsNullOrEmpty(_rootStorageDirectory)) _rootStorageDirectory = Properties.Settings.Default.RootStorage;
                if (string.IsNullOrEmpty(_rootStorageDirectory)) throw new Exception("Root Storage Directory not set");

                return _rootStorageDirectory;
            }
            set { _rootStorageDirectory = value; }
        }

        public static void AddAuditEntry(BroadvineContext context, AuditSource sourceType, AuditType auditType, string information, string details, int? invoiceID, int? vendorID, int? propertyID, string source = "System")
        {
            if (context == null) context = new BroadvineContext();
            AuditLog entry = new AuditLog()
            {
                DateTimeCreated = DateTime.Now,
                AuditSourceID = sourceType,
                AuditTypeID = auditType,
                Information = information,
                Details = details,
                InvoiceID = invoiceID, 
                VendorID = vendorID,
                PropertyID = propertyID,
                Source = source,
                CustomerID = Broadvine.Common.Customer.Current.CustomerID
            };
            context.AuditLog.Add(entry);
            context.SaveChanges();
        }
    }
}