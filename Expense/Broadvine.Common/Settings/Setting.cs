﻿using System.Linq;
using System.Runtime.Caching;

namespace Broadvine.Common.Settings
{
    public class Setting 
    {
        public static string GetSetting (string category, string key, string defaultValue = "")
        {
            string cacheKey = string.Format("{0}{1}", category, key);
            var value = MemoryCache.Default[cacheKey];
            if (value == null)
            {
                using (var context = new Broadvine.DAL.BroadvineContext())
                {
                    var v = context.Settings.FirstOrDefault(s => s.Category == category && s.Key == key);
                    if (v != null) value = v.Value;
                    else value = defaultValue;
                }
                MemoryCache.Default[cacheKey] = value;
            }

            return value.ToString();
        }

        public static void SetSetting(string category, string key, string value)
        {
            SetSetting(string.Format("{0}{1}", category, key), value);
        }

        public static object GetSetting(string cacheKey, object defaultValue = null)
        {
            var value = MemoryCache.Default[cacheKey];
            if (value == null) return defaultValue;
            return value;
        }
        public static void SetSetting(string cacheKey, object value)
        {
            MemoryCache.Default[cacheKey] = value;
        }

    }
}
