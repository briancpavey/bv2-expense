﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broadvine.Models.Common
{
    public class Setting : BaseModel
    {
        public int SettingID { get; set; }
        public string FriendlyName { get; set; }
        public string Category { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        
    }
}
