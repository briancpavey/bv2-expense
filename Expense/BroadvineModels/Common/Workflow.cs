﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Broadvine.Models.Common
{
    public class Workflow : BaseModel
    {
        [Key]
        public int WorkflowID { get; set; }
        public int ModuleID { get; set; }
        public int StatusID { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int? PreviousID { get; set; }
        public int? NextID { get; set; }
        public int? SortOrder { get; set; }
        public bool IsMenuItem { get; set; }
        public string MenuCssClass { get; set; }
    }
}
