﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Broadvine.Models.Common
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string Code { get; set; }
        public Guid Token { get; set; }
        public bool Active { get; set; }

        public Customer()
        {
            this.Token = Guid.NewGuid();
            this.Active = true;
        }
    }
}
