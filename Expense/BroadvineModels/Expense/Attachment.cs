﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Broadvine.Models.Expense
{
    public class Attachment
    {
        public int AttachmentID { get; set; }
        public int InvoiceID { get; set; }
        public int? ParentID { get; set; }
        public string FileLocation { get; set; }
    }
}