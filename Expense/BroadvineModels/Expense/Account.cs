﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Broadvine.Models.Expense
{
    public class Account : BaseModel
    {
        [Key]
        public int AccountID { get; set; }
        public int DepartmentID { get; set; }
        public string Code { get; set; }
        public string Suffix { get; set; }
        public string Name { get; set; }
        public Department Department { get; set; }

        [NotMapped]
        public string NameWithCode
        {
            get
            {
                return string.Format("{0} ({1}.{2})", this.Name, this.Code, this.Suffix);
            }
        }
    }
}