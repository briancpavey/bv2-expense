﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Broadvine.Models.Expense
{
    public enum AuditSource
    {
        System = 9,
        User = 0
    }
    public enum AuditType
    {
        Verbose = -1,
        Info = 0,
        Warning = 1,
        Error = 2
    }

    public class AuditLog : BaseModel
    {
        [Key]
        public int AuditLogID { get; set; }
        public AuditSource AuditSourceID { get; set; }
        public AuditType AuditTypeID { get; set; }
        public int? InvoiceID { get; set; }
        public int? VendorID { get; set; }
        public int? PropertyID { get; set; }
        public string UserID { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public string Information { get; set; }
        public string Details { get; set; }
        public string Source { get; set; }
        public virtual Invoice Invoice { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual Property Property { get; set; }
    }
}