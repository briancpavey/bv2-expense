﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Broadvine.Models.Expense
{
    public class InvoiceLine
    {
        [Key]
        public int InvoiceLineID { get; set; }
        public int InvoiceID { get; set; }
        [ForeignKey("Department")]
        public int? DepartmentID { get; set; }
        [ForeignKey("Account")]
        public int? AccountID { get; set; }
        public decimal Quantity { get; set; }
        [DisplayFormatAttribute(DataFormatString = "{0:C2}")]
        public decimal Price { get; set; }
        [DisplayFormatAttribute(DataFormatString = "{0:C2}")]
        public decimal Total { get; set; }
        public bool Taxable { get; set; }
        public string Description { get; set; }
        public virtual Account Account { get; set; }
        public virtual Department Department { get; set; }
    }
}