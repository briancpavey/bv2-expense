﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Broadvine.Models.Expense
{
    public enum InvoiceStatus
    {
        PendingPayment = -3,
        PendingAnswerback = -2,
        Rejected = -1,
        Pending = 0,
        Approved = 1,
        Post = 2,
        Posted = 3,
        Paid = 4,
        Void = 9, 
        Failed = 99
    }

    public class Invoice : BaseModel
    {
        private Guid? _invoiceGuidA = null;
        private Guid? _invoiceGuidB = null;

        [Key]
        public int InvoiceID { get; set; }
        public Guid? InvoiceGuidA
        {
            get
            {
                if (_invoiceGuidA == null) _invoiceGuidA = Guid.NewGuid();
                return _invoiceGuidA;
            }
            set { _invoiceGuidA = value; }
        }
        public Guid? InvoiceGuidB
        {
            get
            {
                if (_invoiceGuidB == null) _invoiceGuidB = Guid.NewGuid();
                return _invoiceGuidB;
            }
            set { _invoiceGuidB = value; }
        }
        public int PropertyID { get; set; }
        public InvoiceStatus? StatusID { get; set; }
        public int VendorID { get; set; }
        public int DocumentID { get; set; }
        [Display(Name ="Invoice #")]
        public string InvoiceNumber { get; set; }
        public string AccountNumber { get; set; }
        [Display(Name ="Invoice Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime InvoiceDate { get; set; }
        [Display(Name = "Due Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DueDate { get; set; }
        public int PostPeriod { get; set; }
        public string Notes { get; set; }
        public string CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
        public Decimal SubTotal { get; set; }
        [Display(Name = "Tax Total")]
        [DisplayFormatAttribute(DataFormatString = "{0:C2}")]
        public Decimal TaxTotal { get; set; }
        [Display(Name = "Shipping Total")]
        [DisplayFormatAttribute(DataFormatString = "{0:C2}")]
        public Decimal ShippingTotal { get; set; }
        [Display(Name ="Total")]
        [DisplayFormatAttribute(DataFormatString = "{0:C2}")]
        public Decimal NetTotal { get; set; }
        public string BuyerFirstName { get; set; }
        public string BuyerLastName { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime? PostedDate { get; set; }
        public string RejectedReason { get; set; }
        public bool DownloadAttachment { get; set; }
        public string URL { get; set; }
        public bool Transmitted { get; set; }
        public DateTime? TransmittedDate { get; set; }
        public bool Answered { get; set; }
        public DateTime? AnsweredDate { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual Property Property { get; set; }
        public virtual ICollection<InvoiceLine> Lines { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public Invoice()
        {
            this.InvoiceGuidA = Guid.NewGuid();
            this.InvoiceGuidB = Guid.NewGuid();
            this.DownloadAttachment = true;
        }

        [NotMapped]
        public string StatusName
        {
            get
            {
                return this.StatusID.ToString();
            }
        }
    }
}