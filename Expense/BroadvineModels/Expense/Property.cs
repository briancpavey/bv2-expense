﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Broadvine.Models.Expense
{
    public class Property : BaseModel
    {
        [Key]
        public int PropertyID { get; set; }
        public string Code { get; set; }
        [Display(Name="Property Name")]
        public string Name{ get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public bool Active { get; set; }

        [NotMapped]
        public string NameWithCode
        {
            get
            {
                return string.Format("{0} ({1})", this.Name, this.Code);
            }
        }

    }
}