﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.Common.Helpers;

namespace ImportFBInvoices
{
    public class FoodBuyInvoiceFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;

        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        private Dictionary<int, FoodBuyInvoiceHeader> _invoices;

        public string SourceFolder { get; set; }
        public string ArchiveFolder { get; set; }
        public string AnswerBackFolder { get; set; }
        public bool IgnoreIfExists { get; set; }
        public string CustomerToken { get; set; }

        private int CustomerID { get; set; }
        protected FoodBuyInvoiceHeader GetInvoiceHeader(string[] cols)
        {
            int invoiceId = int.Parse(cols[0]);

            if (!_invoices.ContainsKey(invoiceId))
            {
                _invoices.Add(invoiceId, new FoodBuyInvoiceHeader(cols));
            }
            return _invoices[invoiceId];
        }

        public void Process()
        {
            if (string.IsNullOrEmpty(this.SourceFolder)) throw new Exception("Source folder not defined");
            if (string.IsNullOrEmpty(this.ArchiveFolder)) throw new Exception("Archive folder not defined");
            if (string.IsNullOrEmpty(this.AnswerBackFolder)) throw new Exception("Answerback folder not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");

            Broadvine.Common.Customer.Current = Customer.Get(this.CustomerToken);
            if (Broadvine.Common.Customer.Current == null) throw new Exception("Customer is invalid");
            this.CustomerID = Broadvine.Common.Customer.Current.CustomerID;

            Core.AddAuditEntry(null,
                AuditSource.System,
                AuditType.Info,
                "Processing Invoices", "", null, null, null, "FB Invoices");

            var files = Directory.GetFiles(this.SourceFolder).Select(fn => new FileInfo(fn)).OrderBy(f => f.CreationTime);

            foreach (FileInfo f in files)
            {
                this.ProcessInvoiceFile(f.FullName);
            }
        }

        private void ProcessInvoiceFile(string folderFileName)
        {
            Out(string.Format("Processing {0}", folderFileName));

            try
            {
                _invoices = new Dictionary<int, FoodBuyInvoiceHeader>();

                string[] content = File.ReadAllLines(folderFileName);

                foreach (string row in content)
                {
                    string[] cols = row.Split('\t');
                    int invoiceID = int.Parse(cols[0]);

                    FoodBuyInvoiceHeader header = GetInvoiceHeader(cols);
                    header.AddLine(cols);
                }

                this.Save();
                this.SaveAnswerBackFile(this.AnswerBackFolder);
            }
            catch  (Exception ex)
            {
                Out(string.Format("ERROR - {0}", ex.Message));
            }


            this.ArchiveFile(folderFileName);
        }

        private void Save()
        {

            using (var context = new BroadvineContext())
            {

                foreach (int k in this._invoices.Keys)
                {
                    FoodBuyInvoiceHeader header = _invoices[k];
                    header.SaveInvoice(context, this.CustomerID, this.IgnoreIfExists);
                }
            }
        }

        public void SaveAnswerBackFile(string folder)
        {
            StringBuilder sb = new StringBuilder();

            foreach (int k in this._invoices.Keys)
            {
                FoodBuyInvoiceHeader header = _invoices[k];
                if (header.AnswerBackStatus == "SKIP") continue;

                sb.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}", header.InvoiceID, header.InvoiceNumber, header.AnswerBackStatus, header.AnswerBackMessage));
                header = null;
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                string folderFilename = string.Format("{0}\\ACK_{1}.txt", folder, DateTime.Now.ToString("MMddyyHHmmssff"));

                File.WriteAllText(folderFilename, sb.ToString());

                Core.AddAuditEntry(null,
                    AuditSource.System,
                    AuditType.Info,
                    "Answerback created",
                    string.Format("Filename: {0}", folderFilename),
                    null, null, null);
            }
        }

        private string ArchiveFile(string f)
        {
            string d = f.Replace(System.IO.Path.GetFileNameWithoutExtension(f),
                                string.Format("{0}_{1}", System.IO.Path.GetFileNameWithoutExtension(f),
                                System.Guid.NewGuid().ToString()));
            d = string.Format(@"{0}\{1}", this.ArchiveFolder, System.IO.Path.GetFileName(d));

            try
            {
                System.IO.File.Move(f, d);
            }
            catch (Exception e)
            {

            }
            return d;
        }
    }

    public class FoodBuyInvoiceHeader
    {
        public int InvoiceID { get; set; }
        public string PropertyCode { get; set; }
        private string _vendorCode = "";
        public string VendorCode
        {
            get { return int.Parse(_vendorCode).ToString("00000000"); }
            set { _vendorCode = value; }
        }

        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal TotalInvoiceAmount { get; set; }
        public decimal TotalInvoiceSalesTax { get; set; }
        public decimal TotalInvoiceShippingAmount
        {
            get
            {
                if (this._lines == null) return 0;
                return this._lines.Sum(l => l.ShippingAmount);
            }

        }
        public string AttachmentURL { get; set; }
        public string BuyerFirstName { get; set; }
        public string BuyerLastName { get; set; }
        public decimal SubTotal
        {
            get
            {
                return this._lines.Sum(l => l.LineAmount);
            }
        }
        public FoodBuyInvoiceHeader(string[] cols)
        {
            //  Setup the primary header information 
            this.InvoiceID = int.Parse(cols[0]);
            this.PropertyCode = cols[1].Trim();
            this.VendorCode = cols[2].Trim();
            this.InvoiceNumber = cols[3].Trim();
            this.InvoiceDate = DateTime.Parse(cols[4].Trim());
            this.TotalInvoiceAmount = decimal.Parse(cols[5].Trim());
            this.TotalInvoiceSalesTax = decimal.Parse(cols[6].Trim());
            this.AttachmentURL = cols[13].Trim();
            this.BuyerFirstName = cols[14].Trim();
            this.BuyerLastName = cols[15].Trim();
        }
        public string AnswerBackStatus { get; set; }
        public string AnswerBackMessage { get; set; }

        private List<FoodBuyInvoiceLine> _lines = new List<FoodBuyInvoiceLine>();
        public void AddLine(string[] cols)
        {
            this._lines.Add(new FoodBuyInvoiceLine(cols));
        }

        public void SaveInvoice(BroadvineContext context, int customerId, bool ignoreIfExists = false)
        {
            this.AnswerBackStatus = "PASS";
            int postingPeriod = 201701;
            bool isNew = false;

            try
            {
                Property property = context.Properties.FirstOrDefault(p => p.Code == this.PropertyCode);
                if (property == null) throw new Exception(string.Format("Property Code '{0}' does not exist", this.PropertyCode));

                Vendor vendor = context.Vendors.FirstOrDefault(v => v.Code == this.VendorCode);
                if (vendor == null) throw new Exception(string.Format("Vendor Code '{0}' does not exist", this.VendorCode));

                Invoice invoice = context.Invoices.FirstOrDefault(i => i.DocumentID == this.InvoiceID);
                if (invoice == null) { invoice = new Invoice() { PostPeriod = postingPeriod }; isNew = true; }

                if (!isNew && ignoreIfExists)
                {
                    this.AnswerBackStatus = "SKIP"; return;
                }

                invoice.DocumentID = this.InvoiceID;
                invoice.InvoiceNumber = this.InvoiceNumber;
                invoice.InvoiceDate = this.InvoiceDate;
                invoice.DueDate = this.InvoiceDate;             //  Need due date 
                invoice.SubTotal = this.SubTotal;
                invoice.TaxTotal = this.TotalInvoiceSalesTax;
                invoice.NetTotal = this.TotalInvoiceAmount;
                invoice.ShippingTotal = this.TotalInvoiceShippingAmount;
                invoice.PropertyID = property.PropertyID;
                invoice.VendorID = vendor.VendorID;
                invoice.ReceivedDate = DateTime.Now;
                invoice.URL = this.AttachmentURL;
                invoice.BuyerFirstName = this.BuyerFirstName;
                invoice.BuyerLastName = this.BuyerLastName;
                invoice.StatusID = InvoiceStatus.Pending;
                invoice.Transmitted = false;
                invoice.CustomerID = customerId;

                if (invoice.InvoiceID == 0) context.Invoices.Add(invoice);

                //  Save the invoice header 
                context.SaveChanges();

                //  Remove any existing line items from this invoice
                context.InvoiceLines.RemoveRange(context.InvoiceLines.Where(i => i.InvoiceID == invoice.InvoiceID));
                context.SaveChanges();

                //  Enumerate the line items an save them 
                foreach (FoodBuyInvoiceLine l in this._lines)
                {
                    Department department = context.Departments.FirstOrDefault(d => d.Code == l.DepartmentCode && d.CustomerID == customerId);
                    //if (department == null) throw new Exception(string.Format("Department Code '{0}' does not exist", l.DepartmentCode));

                    Account account = context.Accounts.FirstOrDefault(a => a.Code == l.AccountCode && a.CustomerID == customerId);
                    if (account == null) throw new Exception(string.Format("Account Code '{0}' does not exist", l.AccountCode));

                    InvoiceLine line = new InvoiceLine()
                    {
                        DepartmentID = department == null ? 0 : department.DepartmentID,
                        AccountID = account.AccountID,
                        Description = "",
                        Taxable = l.UseTax,
                        Quantity = 1,
                        Price = l.LineAmount,
                        Total = l.LineAmount,
                        InvoiceID = invoice.InvoiceID,
                    };
                    context.InvoiceLines.Add(line);
                }

                context.SaveChanges();

                Core.AddAuditEntry(null,
                                AuditSource.System,
                                AuditType.Info,
                                (isNew ? "Invoice Created" : "Invoice updated"),
                                ""
                                , invoice.InvoiceID, vendor.VendorID, property.PropertyID);

            }
            catch (Exception e)
            {
                this.AnswerBackStatus = "FAIL";
                this.AnswerBackMessage = e.Message;

                Core.AddAuditEntry(null,
                                AuditSource.System,
                                AuditType.Info,
                                string.Format("Processing Failed - {0}", this.InvoiceNumber),
                                e.Message,
                                null, null, null);
            }
        }
    }

    public class FoodBuyInvoiceLine
    {
        public bool UseTax { get; set; }
        public string DepartmentCode { get; set; }
        public string AccountCode { get; set; }
        public decimal SalesTaxAmount { get; set; }
        public decimal ShippingAmount { get; set; }
        public decimal LineAmount { get; set; }
        public string AttachmentURL { get; set; }

        public FoodBuyInvoiceLine(string[] cols)
        {
            this.UseTax = cols[7].Trim() != "0";
            this.DepartmentCode = cols[8].Trim();
            this.AccountCode = cols[9].Trim();
            this.SalesTaxAmount = decimal.Parse(cols[10].Trim());
            this.ShippingAmount = decimal.Parse(cols[11].Trim());
            this.LineAmount = decimal.Parse(cols[12].Trim());
            this.AttachmentURL = cols[13].Trim();
        }
    }
}
