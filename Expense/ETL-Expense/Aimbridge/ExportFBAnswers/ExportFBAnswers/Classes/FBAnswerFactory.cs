﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.Common.Helpers;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.DAL;

namespace ExportFBAnswers
{
    public class FBAnswerFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;

        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        public string DestinationFolder { get; set; }
        public string CustomerToken { get; set; }
        public void ProcessRejections()
        {
            if (string.IsNullOrEmpty(this.DestinationFolder)) throw new Exception("Destination folder not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");

            Broadvine.Common.Customer.Current = Customer.Get(this.CustomerToken);
            if (Broadvine.Common.Customer.Current == null) throw new Exception("Customer is invalid");

            int customerId = Broadvine.Common.Customer.Current.CustomerID;

            StringBuilder sb = new StringBuilder();
            using (var context = new BroadvineContext())
            {
                //  Only invoices that are marked as reject and have not been transmitted 
                List<Invoice> invoices = context.Invoices.Where(i => i.StatusID == InvoiceStatus.Rejected && i.Transmitted == false && i.CustomerID == customerId).ToList();

                Out(string.Format("{0} rejection(s) found", invoices.Count.ToString()));
                Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, "Transmitting Rejections", string.Format("{0} rejections(s) discovered", invoices.Count), null, null, null, "FB Reject");

                foreach (Invoice invoice in invoices)
                {
                    try
                    {
                        string reason = invoice.RejectedReason.Replace(Environment.NewLine, " ");
                        sb.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}", invoice.DocumentID, invoice.InvoiceNumber, "REJECT", reason));
                    }
                    catch (Exception ex)
                    {
                        Out(string.Format("ERROR - {0}", ex.Message));
                        Core.AddAuditEntry(context, AuditSource.System, AuditType.Error, "Error", ex.Message, invoice.InvoiceID, null, null, "FB Reject");
                    }

                    invoice.Transmitted = true;
                    context.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                }

                context.SaveChanges();

                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    string folderFilename = string.Format("{0}REJ_{1}.txt", this.DestinationFolder, DateTime.Now.ToString("MMddyyyyHHmmssff"));
                    File.WriteAllText(folderFilename, sb.ToString());
                }
            }
        }
    }
}
