﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.Common.Helpers;

namespace ImportJDEPayments
{
    public class JDEPaymentFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;
        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        public string SourceFolder { get; set; }
        public string ArchiveFolder { get; set; }
        public string CopyToFolder { get; set; }
        public string CustomerToken { get; set; }
        private int CustomerID { get; set; }
        public void Process()
        {
            if (string.IsNullOrEmpty(this.SourceFolder)) throw new Exception("Source folder not defined");
            if (string.IsNullOrEmpty(this.ArchiveFolder)) throw new Exception("Archive folder not defined");
            if (string.IsNullOrEmpty(this.CopyToFolder)) throw new Exception("CopyTo folder not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");

            Broadvine.Common.Customer.Current = Customer.Get(this.CustomerToken);
            if (Broadvine.Common.Customer.Current == null) throw new Exception("Customer is invalid");
            this.CustomerID = Broadvine.Common.Customer.Current.CustomerID;

            var files = Directory.GetFiles(this.SourceFolder).Select(f => new FileInfo(f)).OrderBy(f => f.CreationTime);
            if (files == null || files.Count() == 0)
            {
                return;
            }
            else
            {
                Core.AddAuditEntry(null, AuditSource.System, AuditType.Info, "Processing Payments", "", null, null, null, "JDE Payment");
                using (var context = new BroadvineContext())
                {
                    foreach (FileInfo file in files)
                        if (Path.GetFileName(file.FullName).ToUpper().StartsWith("PAY_"))
                            ProcessAnswerback(context, file);
                }
            }
        }
        private void ProcessAnswerback(BroadvineContext context, FileInfo f)
        {
            Out(string.Format("Filename: {0}", f.FullName));
            string[] content = File.ReadAllLines(f.FullName);
            Out(string.Format("Discovered {0} payment(s)", content.Length));

            Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, string.Format("Discovered {0} payment(s)", content.Length), Path.GetFileName(f.FullName), null, null, null, "JDE payment");
            Dictionary<string, string> payments = new Dictionary<string, string>();

            foreach (string row in content)
            {
                if (row.Length < 135) continue;
                JDEPayment payment = new JDEPayment(row);
                if (payment.ProcessPayment(context, this.CustomerID))
                {
                    Out(string.Format("Document ID: {0}", payment.DocumentID.ToString()));
                    if (!payments.ContainsKey(payment.DocumentID)) payments.Add(payment.DocumentID, "");
                    payments[payment.DocumentID] = row;
                }
            }

            if (payments.Count > 0)
            {
                string folderFilename = string.Format("{0}PAY_{1}.txt", this.CopyToFolder, DateTime.Now.ToString("MMddyyyyHHmmss"));
                StringBuilder paymentContent = new StringBuilder();

                foreach (string key in payments.Keys)
                    paymentContent.AppendLine(payments[key].Trim());

                File.WriteAllText(folderFilename, paymentContent.ToString());
            }

            MoveFile(f.FullName, true);
        }

        private void MoveFile(string f, bool passed)
        {
            string d = f.Replace(System.IO.Path.GetFileNameWithoutExtension(f),
                                string.Format("{0}_{1}", System.IO.Path.GetFileNameWithoutExtension(f), System.Guid.NewGuid().ToString()));
            d = string.Format(@"{0}\{1}", this.ArchiveFolder, System.IO.Path.GetFileName(d));

            try
            {
                System.IO.File.Move(f, d);
            }
            catch (Exception e)
            {
            }
        }

        private string DateToJulianDate(DateTime date)
        {
            return (1000 * (date.Year - 2000 + 100) + date.DayOfYear).ToString();
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }

    public class JDEPayment
    {
        public string CheckNumber = "";
        public DateTime CheckDate;
        public string InvoiceNumber = "";
        public string CheckTotalSign = "";
        public decimal CheckTotal = 0;
        public string PropertyTotalsign = "";
        public decimal PropertyTotal = 0;
        public string Void = "";
        public string FileLineID = "";
        public string DocumentID = "";
        public string PropertyCode = "";
        public string VendorNumber = "";
        public string Account = "";

        public JDEPayment(string line)
        {

            this.CheckNumber = line.FixedValue(0, 8);
            this.CheckDate = line.FixedValue(8, 8).JDEDateValue();
            this.InvoiceNumber = line.FixedValue(16, 30);
            this.CheckTotalSign = line.FixedValue(46, 1);
            this.CheckTotal = decimal.Parse(line.FixedValue(47, 14));
            this.PropertyTotalsign = line.FixedValue(61, 1);
            this.PropertyTotal = decimal.Parse(line.FixedValue(62, 14));
            this.Void = line.FixedValue(76, 1);
            this.FileLineID = line.FixedValue(77, 5);
            this.DocumentID = line.FixedValue(82, 25);
            this.PropertyCode = line.FixedValue(107, 12);
            this.VendorNumber = line.FixedValue(119, 8);
            this.Account = line.FixedValue(127, 8);
        }

        public bool ProcessPayment(BroadvineContext context, int customerID)
        {
            if (string.IsNullOrEmpty(this.DocumentID)) return false;

            bool result = false;

            int documentID = int.Parse(this.DocumentID);

            Invoice invoice = context.Invoices.FirstOrDefault(i => i.DocumentID == documentID && i.CustomerID == customerID);
            if (invoice == null)
            {
                Core.AddAuditEntry(context, AuditSource.System, AuditType.Error, string.Format("INVOICE NOT FOUND. CUSTOMER INVOICE DOCUMENT ID {0}", this.DocumentID), "", null, null, null, "JDE payment");
                return result;
            }

            try
            {
                if (string.Compare(this.Void, "Y", true) == 0 && (invoice.StatusID != InvoiceStatus.Void || invoice.StatusID == InvoiceStatus.Posted))
                {
                    string originalCheckNumber = invoice.CheckNumber;
                    invoice.Notes += string.Format("{0}JDE: VOID PAYMENT #{1}", Environment.NewLine, originalCheckNumber);
                    invoice.CheckNumber = "";
                    invoice.CheckDate = DateTime.Parse("1900-01-01");
                    invoice.StatusID = InvoiceStatus.Void;
                    result = true;

                    Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, "Void", string.Format("CHK# {0}", originalCheckNumber), invoice.InvoiceID, invoice.VendorID, invoice.PropertyID, "JDE payment");
                }
                else if (invoice.StatusID == InvoiceStatus.Posted || invoice.StatusID == InvoiceStatus.Void)
                {
                    invoice.Notes += string.Format("{0}[{1} JDE - PAYMENT]{0}", Environment.NewLine, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) +
                                                string.Format("CHECK # {0} DATE: {1}, AMOUNT: {2}", this.CheckNumber, this.CheckDate.ToShortDateString(), (this.CheckTotal != 0 ? this.CheckTotal / 100 : 0));
                    invoice.CheckNumber = this.CheckNumber;
                    invoice.CheckDate = this.CheckDate;
                    invoice.StatusID = InvoiceStatus.Paid;
                    result = true;

                    Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, "Payment", string.Format("CHECK # {0} DATE: {1}, AMOUNT: {2}", this.CheckNumber, this.CheckDate.ToShortDateString(), (this.CheckTotal != 0 ? this.CheckTotal / 100 : 0)),
                                                    invoice.InvoiceID, invoice.VendorID, invoice.PropertyID, "JDE payment");
                }

                if (result) context.SaveChanges();
            }
            catch (Exception e)
            {
                Core.AddAuditEntry(context, AuditSource.System, AuditType.Error, "PAY ERROR", e.Message, invoice.InvoiceID, null, null, "JDE payment");
            }

            invoice = null;

            return result;
        }
    }
    public static class JDEExtensions
    {
        public static string FixedValue(this string s, int startPosition, int length)
        {
            if (startPosition + length > s.Length) return "";
            return s.Substring(startPosition, length).Trim();
        }

        public static DateTime JDEDateValue(this string s)
        {
            return DateTime.Parse(string.Format("{0}/{1}/{2}", s.Substring(0, 2), s.Substring(2, 2), s.Substring(4)));
        }
    }

}
