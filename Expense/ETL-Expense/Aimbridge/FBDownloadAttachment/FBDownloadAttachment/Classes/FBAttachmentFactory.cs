﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using WebSupergoo.ImageGlue7;
using Broadvine.Common;
using Broadvine.Models.Expense;
using Broadvine.DAL;
using Broadvine.Common.Helpers;

namespace FBDownloadAttachment
{
    public class FBAttachmentFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;
        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        public string DownloadFolder { get; set; }
        public string StorageFolder { get; set; }
        public string LicenseKey { get; set; }
        public string CustomerToken { get; set; }
        public void Process()
        {
            Out("Processing...");
            if (string.IsNullOrEmpty(this.DownloadFolder)) throw new Exception("Download folder not defined");
            if (string.IsNullOrEmpty(this.StorageFolder)) throw new Exception("Storage folder not defined");
            if (string.IsNullOrEmpty(this.LicenseKey)) throw new Exception("License key not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");
            
            Broadvine.Models.Common.Customer customer = Customer.Get(this.CustomerToken);
            if (customer == null) throw new Exception("Customer is invalid");
            Broadvine.Common.Customer.Current = customer;

            int customerId = customer.CustomerID;

            using (BroadvineContext context = new BroadvineContext())
            {
                List<Invoice> invoices = context.Invoices.Where(i => i.DownloadAttachment == true && i.CustomerID == customerId).ToList();

                Console.WriteLine(string.Format("Discovered {0} Images...", invoices.Count()));
                foreach (Invoice invoice in invoices)
                {
                    Out(string.Format("Invoice # {0} / URL: {1}", invoice.InvoiceNumber, invoice.URL));
                    string folderFilename = DownloadAttachment(invoice);
                    Out(string.Format("Filename: {0}", folderFilename));
                    
                    if (!string.IsNullOrEmpty(folderFilename) && File.Exists(folderFilename))
                    {
                        if (ProcessFile(context, invoice, folderFilename))
                        {
                            invoice.DownloadAttachment = false;
                            context.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();
                        }
                    }
                }
            }
        }

        public string DownloadAttachment(Invoice i)
        {
            string downloadFolderFilename = "";
            string filename = i.URL.Substring(i.URL.IndexOf("=") + 1);

            if (!string.IsNullOrEmpty(filename))
            {
                downloadFolderFilename = string.Format("{0}{1}", this.DownloadFolder, filename);


                Console.WriteLine(string.Format("Downloading: {0}", downloadFolderFilename));
                using (WebClient client = new WebClient())
                {
                    try
                    {
                        client.DownloadFile(i.URL, downloadFolderFilename);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.InnerException.Message);
                        downloadFolderFilename = "";
                        Core.AddAuditEntry(null, AuditSource.System, AuditType.Error, "Image Processing", string.Format("{1}{0}{2}", Environment.NewLine, ex.Message, i.URL), i.InvoiceID, null, null, "Image Processing");
                    }
                }
            }

            return downloadFolderFilename;
        }

        public bool ProcessFile(BroadvineContext context, Invoice invoice, string sourceFolderFilename)
        {
            bool result = true;

            if (System.IO.Path.GetExtension(sourceFolderFilename).ToUpper() != ".PDF") return true;

            XSettings.InstallLicense(this.LicenseKey);
            XSettings.LogErrors = false;
            
            if (File.Exists(sourceFolderFilename))
            {
                string destinationFolder = string.Format("{0}{1}{2}\\", this.StorageFolder, DateTime.Now.Year, DateTime.Now.Month.ToString("00"));
                if (!Directory.Exists(destinationFolder)) Directory.CreateDirectory(destinationFolder);

                destinationFolder = string.Format("{0}{1}\\", destinationFolder, invoice.InvoiceID.ToString("0000000"));
                if (!Directory.Exists(destinationFolder)) Directory.CreateDirectory(destinationFolder);
                try
                {
                    using (XImage i = XImage.FromFile(sourceFolderFilename))
                    {
                        for (int p = 1; p <= i.FrameCount; p++)
                        {
                            using (Canvas canvas = new Canvas())
                            {
                                canvas.Export.Quality = 100;

                                i.Frame = p;
                                DrawOptions d = new DrawOptions();
                                d.ImageFit = DrawOptions.ImageFitType.AspectRatio;
                                d.HRes = d.HRes ?? 72 * 2;
                                d.VRes = d.VRes ?? 72 * 2;
                                canvas.DrawImage(i, d);

                                byte[] buffer = canvas.GetAs("jpg");
                                string filename = string.Format("{0}PAGE_{1}.jpg", destinationFolder, p);

                                File.WriteAllBytes(filename, buffer);

                                Attachment attachment = new Attachment();
                                attachment.InvoiceID = invoice.InvoiceID;
                                attachment.FileLocation = filename.Replace(this.StorageFolder, "");

                                context.Attachments.Add(attachment);
                                context.SaveChanges();
                            }
                        }
                    }

                    File.Delete(sourceFolderFilename);
                }
                catch (Exception e)
                {
                    Out(string.Format("ERROR - {0}", e.Message));
                }
            } else
            {
                result = false;
            }


            return result;
        }
    }
}
