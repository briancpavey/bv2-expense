﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBDownloadAttachment
{
    class Program
    {
        private static string _APPLICATION_FOLDER = string.Format("{0}\\", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));

        static void Main(string[] args)
        {
            try
            {
                FBAttachmentFactory f = new FBAttachmentFactory();
                f.DownloadFolder = Properties.Settings.Default.DOWNLOAD_FOLDER;
                f.StorageFolder = Properties.Settings.Default.STORAGE_FOLDER;
                f.LicenseKey = Properties.Settings.Default.IG_LICENSE_KEY;
                f.CustomerToken = Properties.Settings.Default.CUSTOMER_TOKEN;
                f.LogIt += Log;
                f.Process();
                f.LogIt -= Log;

            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }
        private static void Log(string m)
        {
            m = string.Format("{0} - {1}{2}", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), m, Environment.NewLine);
            Console.WriteLine(m);
            System.IO.File.AppendAllText(string.Format("{0}Log.txt", _APPLICATION_FOLDER), m);
        }
    }
}
