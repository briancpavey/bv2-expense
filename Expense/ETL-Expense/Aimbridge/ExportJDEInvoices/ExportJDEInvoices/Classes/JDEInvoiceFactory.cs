﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.Common.Helpers;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.DAL;

namespace ExportJDEInvoices
{
    public class JDEInvoiceFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;
        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        public string DestinationFolder { get; set; }
        public string CustomerToken { get; set; }
        public void Process()
        {
            if (string.IsNullOrEmpty(this.DestinationFolder)) throw new Exception("Destination folder not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");

            Broadvine.Models.Common.Customer customer = Customer.Get(this.CustomerToken);
            if (customer == null) throw new Exception("Customer is invalid");
            Broadvine.Common.Customer.Current = customer;

            int customerId = customer.CustomerID;

            StringBuilder sb = new StringBuilder();
            using (var context = new BroadvineContext())
            {
                //  Only invoices that are marked as post and have not been transmitted 
                List<Invoice> invoices = context.Invoices.Where(i => i.Transmitted == false && i.StatusID == InvoiceStatus.Post && i.CustomerID == customerId).ToList();
                
                Out(string.Format("Found {0} invoice(s)", invoices.Count.ToString()));
                Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, "Transmitting Invoices", string.Format("{0} invoice(s) discovered", invoices.Count), null, null, null, "JDE Export");

                foreach (Invoice invoice in invoices)
                {
                    try
                    {
                        GenerateInvoiceOutput(context, ref sb, invoice);
                    }
                    catch (Exception ex)
                    {
                        Out(string.Format("ERROR - {0}", ex.Message));
                        Core.AddAuditEntry(context, AuditSource.System, AuditType.Error, "Error", ex.Message, invoice.InvoiceID, null, null, "JDE Export");
                    }
                }

                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    string folderFilename = string.Format("{0}INV_{1}.txt", this.DestinationFolder, DateTime.Now.ToString("MMddyyyyHHmmssff"));
                    File.WriteAllText(folderFilename, sb.ToString());
                }
            }
        }

        private void GenerateInvoiceOutput(BroadvineContext context, ref StringBuilder sb, Invoice invoice)
        {
            int counter = 1;

            DateTime transmissionDate = DateTime.Now;

            foreach (InvoiceLine line in invoice.Lines)
            {
                sb.Append(invoice.Property.Code.PadRightLimited(12));                                       //  Property Code 
                sb.Append(invoice.Vendor.Code.PadLeftLimited(8).Replace(" ", "0"));                             //  Vendor Code 
                sb.Append(DateToJulianDate(invoice.InvoiceDate));                                           //  Vendor Invoice Date 
                sb.Append(line.Total.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());                          //  Line Item Amount 
                sb.Append(invoice.InvoiceNumber.PadRightLimited(25));                                       //  Invoice Number  
                sb.Append(" ".PadRightLimited(30));                                                         //  Invoice Comments
                sb.Append(line.Account.Code.PadRightLimited(6));                                            //  Line Item Account Code
                sb.Append(counter.ToString().PadLeftLimited(12).Replace(" ", "0"));                         //  Line Number
                sb.Append(invoice.DocumentID.ToString().PadRightLimited(64));                               //  Invoice Id (This is the one passed to us from Foodbuy)
                sb.Append(invoice.URL.PadRightLimited(100));                                                //  Image file name URL
                sb.Append(" ");                                                                             //  1 BLANK SPACE  
                sb.Append(DateToJulianDate(transmissionDate));                                              //  Transmission date (TODAY)
                sb.Append(invoice.Vendor.Name.PadRightLimited(40));                                         //  Vendor Name 
                sb.Append(invoice.SubTotal.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());                    //  Invoice Subtotal 
                sb.Append(invoice.TaxTotal.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());                    //  Invoice Tax Total 
                sb.Append(invoice.ShippingTotal.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());               //  Invoice Freight/Shipping total 
                sb.Append(invoice.NetTotal.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());                    //  Invoice Grand Total (TODO!!!!)
                sb.Append(DateToJulianDate(invoice.ReceivedDate));                                          //  Invoice Order Date (Creation Date???)
                sb.Append(DateToJulianDate(invoice.DueDate));                                               //  Invoice Due Date
                sb.Append(invoice.BuyerFirstName.PadRightLimited(40));                                      //  Buyer first name 
                sb.Append(invoice.BuyerLastName.PadRightLimited(40));                                       //  Buyer last name 
                sb.Append("".PadRightLimited(25));                                                          //  Merchant Customer Number 
                sb.Append(invoice.Property.Name.PadRightLimited(40));                                       //  Property name 
                sb.Append(invoice.Property.Code.PadRightLimited(12));                                       //  Property code 
                sb.Append(invoice.Property.Address1.PadRightLimited(50));                                   //  Property address 1
                sb.Append(invoice.Property.Address2.PadRightLimited(50));                                   //  Property address 2 
                sb.Append(invoice.Property.City.PadRightLimited(50));                                       //  Property city 
                sb.Append(invoice.Property.State.PadRightLimited(2));                                       //  Property state 
                sb.Append(invoice.Property.ZipCode.PadRightLimited(10));                                    //  Property zip code 
                sb.Append(invoice.Property.Phone.PadRightLimited(16));                                      //  Property phone number 
                sb.Append("".PadLeftLimited(25));                                                           //  Merchant SKU 
                sb.Append("".PadLeftLimited(100));                                                          //  Product description 
                sb.Append("".PadLeftLimited(10));                                                           //  Unit of Measure 
                sb.Append(line.Quantity.ToString().Replace(".", "").PadLeftLimited(10).Replace(" ", "0").ShiftNegativeToFront());                       //  Quantity line item  
                sb.Append(line.Total.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());         //  Unit price line item 
                sb.Append("0".PadLeftLimited(15).Replace(" ", "0"));                                            //  Tax total line item 
                //sb.Append("0".PadLeftLimited(15));                                                          //  Freight/Shipping total line item 
                sb.Append(invoice.ShippingTotal.ToString().Replace(".", "").PadLeftLimited(15).Replace(" ", "0").ShiftNegativeToFront());               //  Invoice Freight/Shipping total 
                sb.Append("".PadLeftLimited(20));                                                           //  Unit number 
                sb.Append(line.Department.Code.PadRightLimited(3));                                         //  Department 
                sb.AppendLine();

                counter++;
            }

            invoice.Transmitted = true;
            invoice.TransmittedDate = transmissionDate;
            context.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();

            Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, "Transmitted", "", invoice.InvoiceID, null, null, "JDE Export");
        }

        private string DateToJulianDate(DateTime date)
        {
            return (1000 * (date.Year - 2000 + 100) + date.DayOfYear).ToString();
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
    public static class JDEExtensions
    {
        public static string FixedValue(this string s, int startPosition, int length)
        {
            if (startPosition + length > s.Length) return "";
            return s.Substring(startPosition, length).Trim();
        }
        public static string PadRightLimited(this string s, int limit)
        {
            if (s.Length > limit) s = s.Substring(0, limit);
            return s.PadRight(limit);
        }

        public static string PadLeftLimited(this string s, int limit)
        {
            if (s.Length > limit) s = s.Substring(0, limit);
            return s.PadLeft(limit);
        }

        public static string ShiftNegativeToFront(this string s)
        {
            if (!s.Contains("-")) return s;
            return "-" + s.Replace('-', '0').Substring(1);
        }
    }
}
