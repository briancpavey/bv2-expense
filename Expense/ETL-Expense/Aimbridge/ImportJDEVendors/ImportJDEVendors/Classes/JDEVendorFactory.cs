﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.Common.Helpers;

namespace ImportJDEVendors
{
    class JDEVendorFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;
        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        public string SourceFolder { get; set; }
        public string ArchiveFolder { get; set; }
        public string CopyToFolder { get; set; }
        public string CustomerToken { get; set; }
        private int CustomerID { get; set; }
        public void Process()
        {
            if (string.IsNullOrEmpty(this.SourceFolder)) throw new Exception("Source folder not defined");
            if (string.IsNullOrEmpty(this.ArchiveFolder)) throw new Exception("Archive folder not defined");
            if (string.IsNullOrEmpty(this.CopyToFolder)) throw new Exception("CopyTo folder not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");

            Broadvine.Common.Customer.Current = Customer.Get(this.CustomerToken);
            if (Broadvine.Common.Customer.Current == null) throw new Exception("Customer is invalid");
            this.CustomerID = Broadvine.Common.Customer.Current.CustomerID;

            Core.AddAuditEntry(null, AuditSource.System, AuditType.Info, "Processing Vendors", "", null, null, null, "JDE Vendors");

            var files = Directory.GetFiles(this.SourceFolder).Select(f => new FileInfo(f)).OrderBy(f => f.CreationTime);
            if (files == null || files.Count() == 0)
            {
                Console.WriteLine(string.Format("No files found in {0}", this.SourceFolder));
                return;
            }
            else
            {
                
                using (var context = new BroadvineContext())
                {
                    foreach (FileInfo file in files) 
                        if (Path.GetFileName(file.FullName).ToUpper().StartsWith("VEN_"))
                            ProcessVendors(context, file.FullName);
                        else
                        {
                            Out(string.Format("Ignoring file - {0}", file.FullName));
                        }
                            
                }
            }
        }

        public void ProcessVendors(BroadvineContext context, string folderFilename)
        {
            Out(string.Format("Filename: {0}", folderFilename));
            
            //  Read the supplied vendor file 
            string[] content = File.ReadAllLines(folderFilename);

            List<JDEVendor> vendors = new List<JDEVendor>();
            foreach (string line in content)
            {
                JDEVendor v = new JDEVendor(line);
                vendors.Add(v);
            }

            Core.AddAuditEntry(context, AuditSource.System, AuditType.Info, "Processing Vendors", string.Format("Filename: {1}{0}Discovered {2} Vendor(s)", Environment.NewLine, System.IO.Path.GetFileName(folderFilename), vendors.Count), null, null, null);

            foreach (JDEVendor vendor in vendors)
            {
                Vendor v = context.Vendors.FirstOrDefault(ven => ven.Code == vendor.Code);
                try
                {
                    if (v == null) v = new Vendor();

                    v.Code = vendor.Code;
                    v.Name = vendor.Name;
                    v.Address1 = vendor.Address1;
                    v.Address2 = vendor.Address2;
                    v.City = vendor.City;
                    v.State = vendor.State;
                    v.ZipCode = vendor.ZipCode;
                    v.Active = vendor.IsActive;
                    v.CustomerID = this.CustomerID;

                    if (v.VendorID > 0)
                    {
                        Out(string.Format("UDPATE VENDOR CODE: {0}", v.Code));
                        context.Entry(v).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        Out(string.Format("ADD VENDOR CODE: {0}", v.Code));
                        context.Vendors.Add(v);
                    }
                    context.SaveChanges();

                }
                catch (Exception e)
                {
                    Out(string.Format("ERROR: {0}", e.Message));
                    Core.AddAuditEntry(context,
                                                    AuditSource.System,
                                                    AuditType.Error,
                                                    "Vendor Error",
                                                    string.Format("Filename: {0}", System.IO.Path.GetFileName(folderFilename)),
                                                    null,
                                                    v.VendorID,
                                                    null);

                }
                finally
                {

                }
            }

            MoveFile(folderFilename, this.ArchiveFolder, false);
            MoveFile(folderFilename, this.CopyToFolder, true);

            vendors = null;
        }

        private void MoveFile(string folderFilename, string destinationFolder, bool remove)
        {
            string d = folderFilename.Replace(System.IO.Path.GetFileNameWithoutExtension(folderFilename),
                                string.Format("{0}_{1}", System.IO.Path.GetFileNameWithoutExtension(folderFilename), remove ? "" : System.Guid.NewGuid().ToString()));
            d = string.Format(@"{0}\{1}", destinationFolder, System.IO.Path.GetFileName(d));

            try
            {
                if (remove) System.IO.File.Move(folderFilename, d);
                else System.IO.File.Copy(folderFilename, d);
            }
            catch (Exception e)
            {
            }
        }
    }
}
