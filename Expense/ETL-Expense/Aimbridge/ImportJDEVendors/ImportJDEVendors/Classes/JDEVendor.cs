﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.Common.Helpers;


namespace ImportJDEVendors
{
    public class JDEVendor
    {
        public string Code = "";
        public string Name = "";
        public string Address1 = "";
        public string Address2 = "";
        public string Address3 = "";
        public string City = "";
        public string State = "";
        public string ZipCode = "";
        public string FileID = "";
        public bool IsActive = true;

        public JDEVendor(string row)
        {
            this.Code = row.FixedValue(0, 8);
            this.Name = row.FixedValue(8, 40);
            this.Address1 = row.FixedValue(48, 40);
            this.Address2 = row.FixedValue(88, 40);
            this.Address3 = row.FixedValue(128, 40);
            this.City = row.FixedValue(168, 40);
            this.State = row.FixedValue(208, 40);
            this.ZipCode = row.FixedValue(248, 40);
            this.FileID = row.FixedValue(288, 11);
            this.IsActive = row.FixedValue(299, 1).Trim().ToUpper() == "Y";
        }
    }
    public static class JDEExtensions
    {
        public static string FixedValue(this string s, int startPosition, int length)
        {
            if (startPosition + length > s.Length) return "";
            return s.Substring(startPosition, length).Trim();
        }
        public static string PadRightLimited(this string s, int limit)
        {
            if (s.Length > limit) s = s.Substring(0, limit);
            return s.PadRight(limit);
        }

        public static string PadLeftLimited(this string s, int limit)
        {
            if (s.Length > limit) s = s.Substring(0, limit);
            return s.PadLeft(limit);
        }

    }
}
