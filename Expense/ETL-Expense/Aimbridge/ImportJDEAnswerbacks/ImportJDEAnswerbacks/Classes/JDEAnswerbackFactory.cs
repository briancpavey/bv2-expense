﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Broadvine.DAL;
using Broadvine.Models.Expense;
using Broadvine.Common;
using Broadvine.Common.Helpers;


namespace ImportJDEAnswerbacks
{
    public class JDEAnswerbackFactory
    {
        public delegate void Log(string t);
        public event Log LogIt;
        public void Out(string t)
        {
            if (LogIt != null) LogIt(t);
        }

        public string SourceFolder { get; set; }
        public string ArchiveFolder { get; set; }
        public string ExceptionsFolder { get; set; }
        public string CustomerToken { get; set; }
        private int CustomerID { get; set; }
        public void Process()
        {
            if (string.IsNullOrEmpty(this.SourceFolder)) throw new Exception("Source folder not defined");
            if (string.IsNullOrEmpty(this.ArchiveFolder)) throw new Exception("Archive folder not defined");
            if (string.IsNullOrEmpty(this.ExceptionsFolder)) throw new Exception("Exceptions folder not defined");
            if (string.IsNullOrEmpty(this.CustomerToken)) throw new Exception("Customer token not defined");

            Broadvine.Common.Customer.Current = Customer.Get(this.CustomerToken);
            if (Broadvine.Common.Customer.Current == null) throw new Exception("Customer is invalid");
            this.CustomerID = Broadvine.Common.Customer.Current.CustomerID;
            
            var files = Directory.GetFiles(this.SourceFolder).Select(fn => new FileInfo(fn)).OrderBy(f => f.CreationTime);

            Core.AddAuditEntry(null, AuditSource.System, AuditType.Info, "Processing Answerbacks", "", null, null, null, "JDE Answer");
            using (var context = new BroadvineContext())
            {
                foreach (FileInfo file in files)
                    if (Path.GetFileName(file.FullName).ToUpper().StartsWith("ACK_"))
                        ProcessAnswerbackFile(context, file.FullName);
            }
        }

        private void ProcessAnswerbackFile(BroadvineContext context, string folderFileName)
        {
            bool success = true;
            Out(string.Format("Filename: {0}", folderFileName));
            Core.AddAuditEntry(context,
                                            AuditSource.System,
                                            AuditType.Info,
                                            "Process Answerback File",
                                            string.Format("Filename: {0}", System.IO.Path.GetFileName(folderFileName)), null, null, null, "JDE Answer");
            try
            {
                string[] content = File.ReadAllLines(folderFileName);

                foreach (string row in content)
                {
                    JDEAnswerbackRecord answer = new JDEAnswerbackRecord(row);

                    Invoice invoice = context.Invoices.FirstOrDefault(i => i.DocumentID == answer.InvoiceId && i.CustomerID == this.CustomerID);
                    if (invoice == null)
                    {
                        Core.AddAuditEntry(context,
                                                AuditSource.System,
                                                AuditType.Warning,
                                                "Invalid Document Id",
                                                string.Format("Filename: {0}", System.IO.Path.GetFileName(folderFileName)), null, null, null);
                        continue;
                    }

                    Out(string.Format("INVOICE # {0}", invoice.InvoiceNumber));

                    if (answer.Status == "PASS") invoice.StatusID = InvoiceStatus.Posted;
                    else
                    {
                        invoice.Notes += string.Format("{0}[{1} JDE - FAILED]{0}", Environment.NewLine, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + answer.ErrorMessage;
                        invoice.StatusID = InvoiceStatus.Failed;
                    }

                    Core.AddAuditEntry(context,
                                                AuditSource.System,
                                                AuditType.Verbose,
                                                invoice.StatusID.ToString(),
                                                answer.ErrorMessage, invoice.InvoiceID, invoice.VendorID, invoice.PropertyID, "JDE Answer");

                    context.Entry(invoice).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                success = false;
                Out(string.Format("ERROR: {0}", ex.Message));
                Core.AddAuditEntry(context, AuditSource.System, AuditType.Error,
                                            "Answerback Error",
                                            string.Format("Filename: {0}", System.IO.Path.GetFileName(folderFileName)) + Environment.NewLine + ex.Message, null, null, null, "JDE Answer");
            }

            MoveFile(folderFileName, success);
        }
        private void MoveFile(string f, bool passed)
        {
            string d = f.Replace(System.IO.Path.GetFileNameWithoutExtension(f),
                                string.Format("{0}_{1}", System.IO.Path.GetFileNameWithoutExtension(f), System.Guid.NewGuid().ToString()));
            d = string.Format(@"{0}\{1}", passed ? this.ArchiveFolder : this.ExceptionsFolder, System.IO.Path.GetFileName(d));

            try
            {
                System.IO.File.Move(f, d);
            }
            catch (Exception e)
            {
                Out(string.Format("ERROR: {0}", e.Message));
            }
        }

    }

    public class JDEAnswerbackRecord
    {
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }

        public JDEAnswerbackRecord(string row)
        {
            this.InvoiceId = int.Parse(row.FixedValue(0, 22));
            this.InvoiceNumber = row.FixedValue(22, 25);
            this.Status = row.FixedValue(47, 4);
            this.ErrorMessage = row.FixedValue(51, 8);
        }
    }

    public static class JDEExtensions
    {
        public static string FixedValue(this string s, int startPosition, int length)
        {
            if (startPosition + length > s.Length) return "";
            return s.Substring(startPosition, length).Trim();
        }
        public static string PadRightLimited(this string s, int limit)
        {
            if (s.Length > limit) s = s.Substring(0, limit);
            return s.PadRight(limit);
        }

        public static string PadLeftLimited(this string s, int limit)
        {
            if (s.Length > limit) s = s.Substring(0, limit);
            return s.PadLeft(limit);
        }

    }

}
