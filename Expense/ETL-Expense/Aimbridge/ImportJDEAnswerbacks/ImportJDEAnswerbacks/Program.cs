﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportJDEAnswerbacks
{
    class Program
    {
        private static string _APPLICATION_FOLDER = string.Format("{0}\\", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));

        static void Main(string[] args)
        {
            Log("PROCESSING STARTED");

            try
            {
                JDEAnswerbackFactory f = new JDEAnswerbackFactory();
                f.SourceFolder = Properties.Settings.Default.SOURCE_FOLDER;
                f.ArchiveFolder = Properties.Settings.Default.ARCHIVE_FOLDER;
                f.ExceptionsFolder = Properties.Settings.Default.EXCEPTION_FOLDER;
                f.CustomerToken = Properties.Settings.Default.CUSTOMER_TOKEN;
                f.LogIt += Log;
                f.Process();
                f.LogIt -= Log;
            }
            catch (Exception ex)
            {
                Log(string.Format("ERROR: {0}", ex.Message));
            }


            Log("PROCESSING COMPLETE");
        }
        private static void Log(string m)
        {
            m = string.Format("{0} - {1}{2}", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), m, Environment.NewLine);
            Console.WriteLine(m);
            System.IO.File.AppendAllText(string.Format("{0}Log.txt", _APPLICATION_FOLDER), m);
        }
    }
}
